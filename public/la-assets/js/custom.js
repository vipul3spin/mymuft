function getcompname(val){
	if(val == "Yes"){
		$('#compname').show();
	} else {
		$('#compname').remove();
	}
	$("#rcomp").prop('disabled', true);
}

function changelang(lang){
	document.getElementById("panel-body").innerHTML = "";
	//alert(lang);
	switch(lang){
		case 'hi':
			$lang = 'hi';
			break;
		case 'en':
			$lang = 'en';
			break;
		default:
			$lang = 'en';
	}
	var langname = '/contact/'+lang+'/main';
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	  if (this.readyState == 4 && this.status == 200) {
		  document.getElementById("panel-body").innerHTML = this.responseText;
		  $('#name').hide();
		  $('#email').hide();
		  $('#mmobno').hide();
		  $('#sbtn').hide();
		  $('#snote').hide();
	  }
	};
	xhttp.open("GET", langname, true);
	xhttp.send();
}

function getservice(serv)
{
  //var serv = document.getElementById("choices").value();
  if(serv == 0){
	document.getElementById("demo").innerHTML = "";
	var lang = document.getElementById("lang").value;
	if(lang == 'hi'){
		alert("कृपया सेवा का चयन करें।");
	} else {
		alert("Please select service you want from us.");
	}
	$('#name').hide();
	$('#email').hide();
	$('#mmobno').hide();
	$('#sbtn').hide();
	$('#snote').hide();

  } else {
	var lang = document.getElementById("lang").value;
	var fname = '/contact/'+lang+'/'+serv;
	document.getElementById("demo").innerHTML = "";
	//$("#demo").html('');

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	  if (this.readyState == 4 && this.status == 200) {
		  if(serv != 5){
		  	document.getElementById("demo").innerHTML = this.responseText;
		  } else {
			document.getElementById("demo").innerHTML = "";
		  }
		  document.getElementById("choices").value = document.getElementById("selchoices").value;
		 // document.getElementById("selchoices").disabled = true;

		//   if(serv != 5){  
			  $('#name').show();
			  $('#email').show();
			  $('#mmobno').show();
			  $('#sbtn').show();
			  $('#snote').show();
		//   } else {
		// 	  $('#name').remove();
		// 	  $('#email').remove();
		// 	  $('#mmobno').remove();
		// 	  $('#sbtn').remove();
		// 	  $('#snote').remove();
			  // $('#name').hide();
			  // $('#email').hide();
			  // $('#mmobno').hide();
			  // $('#sbtn').hide();
		//   }
	  }
	};
	xhttp.open("GET", fname, true);
	xhttp.send();
  }
}

// $(document).ready(function(){
//   $("#servicereq").submit(function(e) {

//     e.preventDefault(); // avoid to execute the actual submit of the form.

//     var form = $(this);

//     $.ajax({
//           type: "POST",
//           url: "sreq.php",
//           data: form.serialize(), // serializes the form's elements.
//           success: function(data)
//           {
// 		document.getElementById("fdatares").innerHTML = "";
//     document.getElementById("fdatares").innerHTML = data;
  

//               //alert(data); // show response from the php script.
//           }
//         });
//   });

// });

$(function () { 
	changelang('en');
  $('#name').hide();
  $('#email').hide();
  $('#mmobno').hide();
  $('#sbtn').hide();
  $('#snote').hide();
  
  $('#err-name').text(""); 
  $('#err-email').text("");
  $('#err-mmobno').text("");

  var loading = $("#loading-overlay");
  $(document).ajaxStart(function () {
	  loading.show();
  });

  $(document).ajaxStop(function () {
	  loading.hide();
  });

  $('#servicereq').on('submit', function (e) {
	e.preventDefault();
	$('#sbtn').hide();
		grecaptcha.ready(function() {
		grecaptcha.execute('6LeqplsaAAAAAH-4lajVewFcyxURRuw2cgOHrj-6', {action: 'submit'}).then(function(token) {
			  $.ajax({
				  url: "/contact",
				  type: "POST",
				  data:  new FormData(document.getElementById("servicereq")),
				  contentType: false,
				  cache: false,
				  processData:false,
				  success: function(dataResult){
					  var dataResult = JSON.parse(dataResult);
					  if(dataResult.statusCode==200){
							//alert("done");
						  var msg1 = dataResult.msg;//"<div id='msssg' class='alert alert-success'>"+ dataResult.msg +"</div>";
						  $('#mssg').append(msg1);
						  document.getElementById("fdatares").innerHTML = ""; 
						  $('#fdatares').hide();
						  $('#lang').hide();
					  }else{
						$('#sbtn').show();
						  if(dataResult.errorname){
							  $('#err-name').text(dataResult.errorname);        				    
						  } else {
							  $('#err-name').text("");
						  }
						  if(dataResult.erroremail){
							  $('#err-email').text(dataResult.erroremail);               				    
						  } else {
							  $('#err-email').text("");
						  }    
						  if(dataResult.errormobileno){
							  $('#err-mmobno').text(dataResult.errormobileno);            				    
						  } else {
							  $('#err-mmobno').text("");
						  } 
						  if(dataResult.errorispplan){
							  $('#err-ispplan').text(dataResult.errorispplan);            				    
						  } else {
							  $('#err-ispplan').text("");
						  }
						  if(dataResult.errorvnamed){
							  $('#err-vnamed').text(dataResult.errorvnamed);            				    
						  } else {
							  $('#err-vnamed').text("");
						  }
						  
						  if(dataResult.erroricapacity){
							  $('#err-icapacity').text(dataResult.erroricapacity);            				    
						  } else {
							  $('#err-icapacity').text("");
						  }

						  if(dataResult.errorvnoc){
							  $('#err-vnoc').text(dataResult.errorvnoc);            				    
						  } else {
							  $('#err-vnoc').text("");
						  }
						  
						  if(dataResult.errorrcomp){
							  $('#err-rcomp').text(dataResult.errorrcomp);            				    
						  } else {
							  $('#err-rcomp').text("");
						  }

						  if(dataResult.errorreisp){
							  $('#err-eisp').text(dataResult.errorreisp);           				    
						  } else {
							  $('#err-eisp').text("");
						  }

						  if(dataResult.errorrbandwidth){
							  $('#err-bandwidth').text(dataResult.errorrbandwidth);           				    
						  } else {
							  $('#err-bandwidth').text("");
						  }		
						  
						  if(dataResult.errorrispl){
							  $('#err-ispl').text(dataResult.errorrispl);            				    
						  } else {
							  $('#err-ispl').text("");
						  }

						  if(dataResult.errorcname){
							  $('#err-cname').text(dataResult.errorcname);            				    
						  } else {
							  $('#err-cname').text("");
						  }

						  if(dataResult.errorbadd){
							  $('#err-badd').text(dataResult.errorbadd);            				    
						  } else {
							  $('#err-badd').text("");
						  }

						  if(dataResult.errorquery){
							  $('#err-query').text(dataResult.errorquery);            				    
						  } else {
							  $('#err-query').text("");
						  }
						  //alert(dataResult.msg);
					  }
					  //setTimeout(function(){
						//$('#mssg').remove();
					  //}, 9000);        				
				  }
			  }); 
		});
		});
  });
});