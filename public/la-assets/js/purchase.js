$(document).ready(function() {
	changelang('en');

	var loading = $("#loading-overlay");
	$(document).ajaxStart(function () {
		loading.show();
	});
  
	$(document).ajaxStop(function () {
		loading.hide();
	});
  
	$('#purchasereq').on('submit', function (e) {
	  e.preventDefault();
	  var serv = document.getElementById("selchoices").value;
	  if(serv != 0){
	  	//alert("Submited");
	  $('#sbtn').hide();
//		  grecaptcha.ready(function() {
//		  grecaptcha.execute('6LeqplsaAAAAAH-4lajVewFcyxURRuw2cgOHrj-6', {action: 'submit'}).then(function(token) {
				$.ajax({
					url: "/purchase-order",
					type: "POST",
					data:  new FormData(document.getElementById("purchasereq")),
					contentType: false,
					cache: false,
					processData:false,
					success: function(dataResult){
						var dataResult = JSON.parse(dataResult);
						if(dataResult.statusCode==200){
							  //alert("done");
							var msg1 = dataResult.msg;//"<div id='msssg' class='alert alert-success'>"+ dataResult.msg +"</div>";
							$('#mssg').append(msg1);
							document.getElementById("fdatares").innerHTML = ""; 
							$('#fdatares').hide();
							$('#lang').hide();
						}else{
						  $('#sbtn').show();

							alert(dataResult.msg);
						}
						//setTimeout(function(){
						  //$('#mssg').remove();
						//}, 9000);        				
					}
				}); 
		//   });
		//   });
	  } else {
		getservice(serv);
	  }
	});

});

function getservice(serv)
{
  //var serv = document.getElementById("choices").value();
  if(serv == 0){
	var lang = document.getElementById("lang").value;
	if(lang == 'hi'){
		alert("कृपया सेवा का चयन करें।");
		$("#err-choices").text("कृपया सेवा का चयन करें।");
	} else {
		alert("Please select service you want to buy.");
		$("#err-choices").text("Please select service you want to buy.");
	}
  } else {
	$("#err-choices").text("");
	$("#choices").val(serv);
  }

}

function changelang(lang){
	document.getElementById("panel-body").innerHTML = "";
	//alert(lang);
	switch(lang){
		case 'hi':
			$lang = 'hi';
			break;
		case 'en':
			$lang = 'en';
			break;
		default:
			$lang = 'en';
	}
	var langname = '/contact/'+lang+'/pmain';
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	  if (this.readyState == 4 && this.status == 200) {
		  document.getElementById("panel-body").innerHTML = this.responseText;
		  print_state("state");
	  }
	};
	xhttp.open("GET", langname, true);
	xhttp.send();
}

function gototab(c,n){
	$("#err-choices").text("");
	var lang = document.getElementById("lang").value;
	var serv = document.getElementById("selchoices").value;
	getservice(serv);
	var flg = false;

	if(lang == 'hi'){
		if(c == 'atab-info'){
			$('#err-name').text("");
			$('#err-cemail').text("");
			$('#err-mmobno').text("");

			if(($('input[name="name"]').val()).length < 5){
				$('#err-name').text("अपना पूरा नाम दर्ज करें");
				flg = true;
			}
			if(IsEmail($('input[name="cemail"]').val()) == false){
				$('#err-cemail').text("कंपनी का ईमेल पता दर्ज करें");
				flg = true;
			}		
			if(($('input[name="mmobno"]').val()).length != 10){
				$('#err-mmobno').text("अपना 10 अंकों का मोबाइल नंबर दर्ज करें");
				flg = true;
			}
		}

		if(c == 'atab-idproof'){
			$('#err-pann').text("");
			$('#err-panp').text("");
			$('#err-idp').text("");

			if($('input[name="pann"]').val() == ""){
				$('#err-pann').text("अपना इंडिविजुअल / कंपनी पैन नंबर डालें");
				flg = true;
			}
			if(IsPAN($('input[name="pann"]').val()) == false){
				$('#err-pann').text("मान्य पैन नंबर दर्ज करें");
				flg = true;
			}
			if($('input[name="panp"]').val() == "" ){
				$('#err-panp').text("कृपया अपने व्यक्तिगत / कंपनी पैन कार्ड की एक फोटोकॉपी अपलोड करें");
				flg = true;
			}		
			if($('input[name="idp"]').val() == "" ){
				$('#err-idp').text("कृपया अपने व्यक्तिगत आईडी कार्ड की एक फोटोकॉपी अपलोड करें");
				flg = true;
			}
		}

		if(c == 'atab-deladdr'){
			$('#err-bandw').text("");
			$('#err-loca').text("");

			if(($('input[name="bandw"]').val()) < 50){
				$('#err-bandw').text("डेटा की मात्रा दर्ज करें ( न्यूनतम 50Mbps )");
				flg = true;
			}
			if($('input[name="loca"]').val() == "" ){
				$('#err-loca').text("सेवा क्षेत्र का स्थान दर्ज करें");
				flg = true;
			}		
		}	

		if(c == 'atab-billdet'){
			$('#err-staddr').text("");
			$('#err-addr').text("");
			$('#err-zipn').text("");
			$('#err-city').text("");
			$('#err-state').text("");
			$('#err-gstn').text("");

			if($('input[name="staddr"]').val() == "" ){
				$('#err-staddr').text("सड़क पता दर्ज करें");
				flg = true;
			}
			if($('input[name="addr"]').val() == "" ){
				$('#err-addr').text("विस्तार पता दर्ज करें");
				flg = true;
			}	
			if($('input[name="zipn"]').val() == "" ){
				$('#err-zipn').text("ज़िप / पोस्टल कोड दर्ज करें");
				flg = true;
			}
			if($('input[name="city"]').val() == "" ){
				$('#err-city').text("शहर का नाम दर्ज करें");
				flg = true;
			}
			if($('input[name="state"]').val() == "" ){
				$('#err-state').text("कृपया राज्य का नाम दर्ज करें");
				flg = true;
			}
			if($('input[name="gstn"]').val() == "" ){
				//$('#err-gstn').text("Please enter GST no.");
				//flg = true;
			}					
		}
	} else {
		if(c == 'atab-info'){
			$('#err-name').text("");
			$('#err-cemail').text("");
			$('#err-mmobno').text("");

			if(($('input[name="name"]').val()).length < 5){
				$('#err-name').text("Enter Your Name");
				flg = true;
			}
			if(IsEmail($('input[name="cemail"]').val()) == false){
				$('#err-cemail').text("Enter Company Email Address");
				flg = true;
			}		
			if(($('input[name="mmobno"]').val()).length != 10){
				$('#err-mmobno').text("Enter your 10 digit mobile number");
				flg = true;
			}
		}

		if(c == 'atab-idproof'){
			$('#err-pann').text("");
			$('#err-panp').text("");
			$('#err-idp').text("");

			if($('input[name="pann"]').val() == ""){
				$('#err-pann').text("Enter your Individual / Company PAN number");
				flg = true;
			}
			if(IsPAN($('input[name="pann"]').val()) == false){
				$('#err-pann').text("Enter Valid PAN number");
				flg = true;
			}
			if($('input[name="panp"]').val() == "" ){
				$('#err-panp').text("Please upload a photocopy of your Individual / Company PAN Card");
				flg = true;
			}		
			if($('input[name="idp"]').val() == "" ){
				$('#err-idp').text("Please upload a photocopy of your personal ID card");
				flg = true;
			}
		}

		if(c == 'atab-deladdr'){
			$('#err-bandw').text("");
			$('#err-loca').text("");

			if(($('input[name="bandw"]').val()) < 50){
				$('#err-bandw').text("Enter amount of data ( Min.50Mbps )");
				flg = true;
			}
			if($('input[name="loca"]').val() == "" ){
				$('#err-loca').text("Enter service area location");
				flg = true;
			}		
		}	

		if(c == 'atab-billdet'){
			$('#err-staddr').text("");
			$('#err-addr').text("");
			$('#err-zipn').text("");
			$('#err-city').text("");
			$('#err-state').text("");
			$('#err-gstn').text("");

			if($('input[name="staddr"]').val() == "" ){
				$('#err-staddr').text("Enter Street Address");
				flg = true;
			}
			if($('input[name="addr"]').val() == "" ){
				$('#err-addr').text("Enter detail address");
				flg = true;
			}	
			if($('input[name="zipn"]').val() == "" ){
				$('#err-zipn').text("Enter Zip/Postal Code");
				flg = true;
			}
			if($('input[name="city"]').val() == "" ){
				$('#err-city').text("Enter City name");
				flg = true;
			}
			if($('input[name="state"]').val() == "" ){
				$('#err-state').text("Please enter State name");
				flg = true;
			}
			if($('input[name="gstn"]').val() == "" ){
				//$('#err-gstn').text("Please enter GST no.");
				//flg = true;
			}					
		}
	}

	if(!flg){
		$("#"+c).removeAttr("href");
		$("#"+c).removeAttr("data-target");
		$("#"+c).removeAttr("data-toggle");
		var cl_next = "#" + n.substring(1, n.length);
		$("#"+n).attr("data-toggle","tab");
		$("#"+n).attr("href",cl_next);
		$("#"+n).attr("data-target",cl_next);
    	$("#"+n).click();
	}
	//$(e).href('#'+e);
}

function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) {
	  return false;
	}else{
	  return true;
	}
}

function IsPAN(pan) {
	var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
	if(!regex.test(pan)) {
	  return false;
	}else{
	  return true;
	}
}

function IsGstn(gstn) {
	var regex = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/;
	if(!regex.test(gstn)) {
	  return false;
	}else{
	  return true;
	}	
}