/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  "use strict";

  //Make the dashboard widgets sortable Using jquery UI
  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs",
    forcePlaceholderSize: true,
    zIndex: 999999
  });
  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

  //jQuery UI sortable for the todo list
  $(".todo-list").sortable({
    placeholder: "sort-highlight",
    handle: ".handle",
    forcePlaceholderSize: true,
    zIndex: 999999
  });

  //bootstrap WYSIHTML5 - text editor
  $(".textarea").wysihtml5();

  $('.daterange').daterangepicker({
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate: moment()
  }, function (start, end) {
    window.alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  });

  /* jQueryKnob */
  $(".knob").knob();

  //jvectormap
// $('#map').vectorMap({  
//   map: 'in_mill',
//   color: '#fff',
//   resize: true,
//   backgroundColor: "transparent",
//     markerStyle: {
//         initial: {
//             fill: '#ed6d23',
//             stroke: '#00589a',
//         }
//     },
//     markers: routers.map(function(h){ return {name: h.name, latLng: h.coords} }),
//     labels: {
//         markers: {
//           render: function(index){
//             return routers[index].name;
//           },
//           offsets: function(index){
//             var offset = routers[index]['offsets'] || [0, 0];
//             return [offset[0] - 7, offset[1] + 3];
//           }
//         }
//     }    
//  });   

  //Sparkline charts
  var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
  $('#sparkline-1').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9"
  });
  myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
  $('#sparkline-2').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9"
  });
  myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
  $('#sparkline-3').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9"
  });

  //The Calender
  $("#calendar").datepicker();

  //SLIMSCROLL FOR CHAT WIDGET
  $('#chat-box').slimScroll({
    height: '250px'
  });

  /* Morris.js Charts */
  
  var monthly = new Morris.Line({
    element: 'chart1',
    resize: true,
    data: monthlydata,
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Leads'],
    lineColors: ['#2ba3f6'],
    behaveLikeLine: true,
    hideHover: 'auto',
    xLabelFormat: function (d) {
      var month = new Array();
        month[0] = "Jan";
        month[1] = "Feb";
        month[2] = "Mar";
        month[3] = "Apr";
        month[4] = "May";
        month[5] = "Jun";
        month[6] = "Jul";
        month[7] = "Aug";
        month[8] = "Sep";
        month[9] = "Oct";
        month[10] = "Nov";
        month[11] = "Dec";
      return ("0" + d.getDate()).slice(-2) + '-' + month[d.getMonth()] + '-' + d.getFullYear();
    },
    hoverCallback: function(index, options, content) {
      var data = options.data[index];
      return("<div class='morris-hover-row-label'>"+data.label+"</div><div class='morris-hover-point' style='color: #2ba3f6'>Leads: "+ data.item1+"</div>");
    },
  });
  
    var weekly = new Morris.Line({
    element: 'chart2',
    resize: true,
    data: weeklydata,
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Leads'],
    lineColors: ['#2ba3f6'],
    behaveLikeLine: true,
    hideHover: 'auto',
    xLabelFormat: function (d) {
      var month = new Array();
        month[0] = "Jan";
        month[1] = "Feb";
        month[2] = "Mar";
        month[3] = "Apr";
        month[4] = "May";
        month[5] = "Jun";
        month[6] = "Jul";
        month[7] = "Aug";
        month[8] = "Sep";
        month[9] = "Oct";
        month[10] = "Nov";
        month[11] = "Dec";
      return ("0" + d.getDate()).slice(-2) + '-' + month[d.getMonth()] + '-' + d.getFullYear();
    },
    hoverCallback: function(index, options, content) {
      var data = options.data[index];
      return("<div class='morris-hover-row-label'>"+data.label+"</div><div class='morris-hover-point' style='color: #2ba3f6'>Leads: "+data.item1+"</div>");
    },
  });
  
    var today = new Morris.Line({
    element: 'chart3',
    resize: true,
    data: todaydata,
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Leads'],
    lineColors: ['#2ba3f6'],
    behaveLikeLine: true,
    hideHover: 'auto',
  });

  //Donut Chart
  //Today unique Login Comparision Chart
  // var todayunq = new Morris.Donut({
  //   element: 'today-unique',
  //   resize: true,
  //   colors: ["#2ba3f6", "#2ecc71"],
  //   data: tuniquelogin,
  //   hideHover: 'auto'
  // });
  
 //Total Login Comparision Chart
  // var totalunq = new Morris.Donut({
  //   element: 'total-unique',
  //   resize: true,
  //   colors: ["#2ba3f6", "#2ecc71"],
  //   data: totuniquelogin,
  //   hideHover: 'auto'
  // });
  
  //   //Voucher Chart
  // var voucher = new Morris.Donut({
  //   element: 'vouchers',
  //   resize: true,
  //   colors: ["#2ba3f6", "#2ecc71"],
  //   data: totalvoucher,
  //   hideHover: 'auto'
  // });
  
  // var otpsentresent = new Morris.Donut({
  //   element: 'otp',
  //   resize: true,
  //   colors: ["#2ba3f6", "#2ecc71"],
  //   data: otpsent,
  //   hideHover: 'auto'
  // });

  //Fix for charts under tabs
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab

  switch (target) {
    case "#weekly-chart":
      weekly.redraw();
      //$(window).trigger('resize');
      break;
    case "#monthly-chart":
      monthly.redraw();
      //$(window).trigger('resize');
      break;
    case "#today-chart":
      today.redraw();
      //$(window).trigger('resize');
      break;
    case "#today-unique":
      todayunq.redraw();
      break;
    case "#total-unique":
      totalunq.redraw();
      break; 
    case "#otp":
      otpsentresent.redraw();
      break; 
    case "#vouchers":
      voucher.redraw();
      break;
  }
});

  /* The todo list plugin */
  $(".todo-list").todolist({
    onCheck: function (ele) {
      window.console.log("The element has been checked");
      return ele;
    },
    onUncheck: function (ele) {
      window.console.log("The element has been unchecked");
      return ele;
    }
  });

});