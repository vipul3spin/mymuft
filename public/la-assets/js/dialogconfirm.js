/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function confirmdelete(id,modl){
    swal({
        title: 'Are you sure?',
        text: "Do you want to delete "+modl+".",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#2eb912',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          swal({
                title :'Deletion in Progress...',
                text: 'We will redirect you shortly.',
                timer: 10000,
                    onOpen: () => {
                        swal.showLoading()
                    }
                });
            document.getElementById("delete-"+modl+"-"+id).submit();
        } else {
            return false;
        }
      });    
} 