@extends('layout')

@section('htmlheader_title') Contact @endsection

@section('main-content')
<div class="row">
    <div class="col-lg-2">
    </div>
    <div class="col-lg-8">
        <div id="mssg" class="col-lg-12" style="display:inline-block;"></div>
            <div class="box box-solid box-success" id="fdatares">
                <form name="servicereq" id="servicereq">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div id="panel-body">
                        <!-- Dyanamic Content form JS -->
                </div>
                </form>
            </div>
    </div>
    <div class="col-lg-2">
    </div>
</div>
@endsection

@push('styles')

@endpush

@push('scripts')
<script src="{{ asset('/la-assets/js/custom.js') }}"></script>
@endpush