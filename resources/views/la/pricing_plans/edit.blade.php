@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/pricing_plans') }}">Pricing Plan</a> :
@endsection
@section("contentheader_description", $pricing_plan->$view_col)
@section("section", "Pricing Plans")
@section("section_url", url(config('laraadmin.adminRoute') . '/pricing_plans'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Pricing Plans Edit : ".$pricing_plan->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($pricing_plan, ['route' => [config('laraadmin.adminRoute') . '.pricing_plans.update', $pricing_plan->id ], 'method'=>'PUT', 'id' => 'pricing_plan-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'plan_fig')
					@la_input($module, 'price')
					@la_input($module, 'ctype')
					@la_input($module, 'terms')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/pricing_plans') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#pricing_plan-edit-form").validate({
		
	});
});
</script>
@endpush
