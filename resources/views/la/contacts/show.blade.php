@extends('la.layouts.app')

@section('htmlheader_title')
	Contact View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<div class="bg-primary clearfix">
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-3">
					<!--<img class="profile-image" src="{{ asset('la-assets/img/avatar5.png') }}" alt="">-->
					<div class="profile-icon text-primary"><i class="fa fa-user"></i></div>
				</div>
				<div class="col-md-9">
					<h4 class="name" id="cont_name">{{ $contact->name }}</h4>
					<div class="desc">{{ $contact->email }}</div>
					<div class="desc">{{ $contact->mmobno }}</div>
					<div class="desc">{{ $contact->vnamed }}</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" style="display:none;">
			<div class="dats1"><div class="label2">Admin</div></div>
			<div class="dats1"><i class="fa fa-envelope-o"></i> superadmin@gmail.com</div>
			<div class="dats1"><i class="fa fa-map-marker"></i> Pune, India</div>
		</div>
		<div class="col-md-1 actions">
			<button class="btn btn-xs btn-edit btn-default" data-toggle="modal" data-target="#AddModal"><i class="fa fa-sticky-note"></i></button>
			<button class="btn btn-xs btn-edit btn-default" data-toggle="modal" data-target="#AddModalEmail"><i class="fa fa-envelope"></i></button>
			@la_access("Contacts", "edit")
				<!-- <a href="{{ url(config('laraadmin.adminRoute') . '/contacts/'.$contact->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br> -->
			@endla_access
			
			@la_access("Contacts", "delete")
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.contacts.destroy', $contact->id], 'method' => 'delete', 'style'=>'display:inline']) }}
					<!-- <button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button> -->
				{{ Form::close() }}
			@endla_access
		</div>
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/contacts') }}" data-toggle="tooltip" data-placement="right" title="Back to Contacts"><i class="fa fa-chevron-left"></i></a></li>
		<!-- <li class=""><a role="tab" data-toggle="tab" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li> -->
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-clock-o"></i> Timeline</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'choice')
						@la_display($module, 'name')
						@la_display($module, 'email')
						@la_display($module, 'mmobno')
						@la_display($module, 'vnamed')
						@la_display($module, 'bandwidth')
						@la_display($module, 'icapacity')
						@la_display($module, 'vnoc')
						@la_display($module, 'rcomp')
						@la_display($module, 'ispl')
						@la_display($module, 'cname')
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane active fade in p20 bg-white" id="tab-timeline">
		{!! Form::open(['action' => 'LA\Contact_TimelinesController@store', 'id' => 'contact_timeline-add-tag-form']) !!}
			<div class="form-group">
			<input name="cont_id" type="hidden" value="{{ $contact->id }}">
			<input type="hidden" name="type" value="Tag">
			<label for="title">Add Tag: </label>
			<input type="text" name="title" id="tag-title" onchange="setTimeout(function(e){onchangebind(this)}, 500, this);" value="" placeholder="Enter Comment" class="form-control">
			<input type="hidden" name="description" value="">
			</div>
		{!! Form::close() !!}
		<ul class="timeline timeline-inverse" id="time-line" style="/*display:none;*/">
		<?php $pdate = '1 Jan 1990'; ?>
		@foreach($ctimeline as $timd)
			{{-- \Carbon\Carbon::parse($timd->created_at)->diffForHumans() --}}	
				<?php
					switch($timd->type){
						case 'Note':
									$clss = 'fa fa-arrow-right bg-green';
									break;
						case 'Tag':
									$clss = 'fa fa-tag bg-yellow';
									break;
						case 'Email':
									$clss = 'fa fa-envelope bg-blue';
									break;
						default:
								$clss = 'fa fa-arrow-right bg-green';
					}
				?>
				<!-- timeline time label -->
				<?php 
					$cdate = \Carbon\Carbon::parse($timd->created_at)->format('d M Y');
					if($pdate != $cdate){
							$pdate = \Carbon\Carbon::parse($timd->created_at)->format('d M Y');
								
				?>
				<li class="time-label">
					<span class="bg-blue">
						{{ \Carbon\Carbon::parse($timd->created_at)->format('d M Y') }}
					</span>
				</li>
				<?php } ?>
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<?php if($timd->deleted_at == "") {
					$delcss = '';
					$dele = true;
				} else {
					$delcss = 'delete';
					$dele = false;
				 } ?>
				<li>
				<i class="<?php echo $clss; ?>"></i>
				
				<div class="timeline-item <?php echo $delcss; ?>" id="timeline-item{{ $timd->id }}">
				
					<span class="time">
					@if($dele)
					{!! Form::open(['route' => [config('laraadmin.adminRoute') . '.contact_timelines.destroy', $timd->id], 'method' => 'delete', 'style'=>'display:inline', 'id'=>'deltimline-cont'.$timd->id]) !!}
					<!-- <i class="fa fa-trash delete" data-id="{{ $timd->id }}"></i> -->
					<input type="hidden" name="id" id="data-id" value="{{ $timd->id }}"> 
					<button class="btn btn-danger btn-xs" type="button" onclick="delitem({{ $timd->id }});"><i class="fa fa-trash"></i></button>
					{!! Form::close() !!}
					@endif
					<i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($timd->created_at)->toTimeString() }}</span>
						
					<?php if($timd->description <> ""){ ?>
						<h3 class="timeline-header" onclick="togglediv({{ $timd->id }});"><b>{{ $timd->title }}</b> . . . </h3>
						<div class="timeline-body" id="timeline-body-{{ $timd->id }}">
						<pre><?php echo html_entity_decode($timd->description); ?></pre>
						</div>
					<?php } else { ?>
						<h3 class="timeline-header" ><b>{{ $timd->title }}</b></h3>
					<?php } ?>
					<div class="timeline-footer" style="display:none;">
						<a class="btn btn-primary btn-xs">Read more</a>
						<a class="btn btn-danger btn-xs">Delete</a>
					</div>
				</div>
				</li>
				@endforeach
				<!-- END timeline item -->
				<li>
				<i class="fa fa-clock-o bg-gray"></i>
				</li>				
			</ul>
			<!--<div class="text-center p30"><i class="fa fa-list-alt" style="font-size: 100px;"></i> <br> No posts to show</div>-->
		</div>
		

	</div>
	<div id="loading-overlay"><img src="{{ url('/la-assets/img/loading.gif') }}"/></div>
</div>

<!-- Add new Action on time line -->
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Note</h4>
			</div>
			{!! Form::open(['action' => 'LA\Contact_TimelinesController@store', 'id' => 'contact_timeline-add--note-form']) !!}
			<input class="form-control" data-rule-maxlength="256" required="1" name="cont_id" type="hidden" value="{{ $contact->id }}" autocomplete="off">
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" name="type" value="Note">
					@la_input($module1, 'title')
					@la_input($module1, 'description')
					{{-- @la_input($module1, 'cont_id') --}}
					{{-- @la_input($module1, 'type') --}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Add', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<!-- Add new email Action on time line -->
<div class="modal fade" id="AddModalEmail" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Send Email</h4>
			</div>
			{!! Form::open(['action' => 'LA\Contact_TimelinesController@store', 'id' => 'contact_timeline-add-email-form']) !!}
			<input class="form-control" required="1" name="cont_id" type="hidden" value="{{ $contact->id }}" autocomplete="off">
			<div class="modal-body">
				<div class="box-body">
						<input type="hidden" name="type" value="Email">

					{{-- @la_input($module1, 'cont_id') --}}
					{{-- @la_input($module1, 'type') --}}
					<div id="emailtype">				
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Send', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
<style>
.profile2 .actions .btn{
	border-radius: 0px;
	width: 49%;
}
.delete{
	background-color: rgb(217, 39, 39, 0.5) !important;
}
.timeline-body{
	display:none;
}
</style>
@endpush

@push('scripts')
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(function() {

	var loading = $("#loading-overlay");
	$(document).ajaxStart(function () {
		loading.show();
	});

	$(document).ajaxStop(function () {
		loading.hide();
	});

    var availableTags = [
		"Agile",
		"Answered",
		"Apply for an ISP license",
		"Berhampore",
		"Big city",
		"Bnr",
		"Busy",
		"Buy Bandwidth for resale",
		"Cable tv network",
		"Call tomorrow",
		"Cannot reach",
		"Chaje",
		"Class A",
		"Class B",
		"Class c license",
		"Class c vno",
		"Continued on whatsapp",
		"Delhi citu",
		"Delhi city",
		"Dugesh ji",
		"Duplicate",
		"Durgesh lead",
		"Email sent",
		"Excitel employee",
		"Existing isp",
		"Fiber",
		"Follow up mail sent",
		"Followed up with",
		"Geetanjali",
		"General information",
		"Gmail company",
		"Gmail contact",
		"Good lead",
		"Google contact",
		"Harsh bhai",
		"High rate",
		"Hotspot",
		"International lead",
		"Ip phone",
		"Isp-license",
		"Junk",
		"Lite",
		"Low budget",
		"Low budget 2lac",
		"MSO licenss","Many locations","Meeting set","Mumbai city","Need setup in pune","Nepal","New business","Next year","No answer","No isp in town","Not for profit","Personal wifi","Potential partner","Regular enquiry","Repeated","Req 1TB","Req25mbps","Required 5gig","Revenue sharing","Sell Bandwidth to ISP Operators","Sent to Karthik","Sent to Moses","Sent to Tonmoy","Sent to durgesh","Sent to oxitel","Sent to vimal","Sent to vipul","Sent to vishva","Small town","Switch off","Talk to someone","Talk to someone at Muft Internet","Technical support","Village","Vishva","Yes","answered","as-number-ip-pool","bnr","buy-bandwidth","call back later","cannot reach","client","cold call","email sent","follow up","http___www_google_com_m8_feeds_groups_info_muftinternet_com_base_329e970f0cdd9827","ip-1-license","isp-license","junk","licensed","list","meeting set","new form","no answer","not interested","osp-license","proposal sent","qualified","sell-bandwidth","sent to Vimal","sent to jinesh","sent to vimal","start isp","start-business","uncontacted","wants software","web","web query","will call back","wrong number",
    ];
    $( "#tag-title" ).autocomplete({
      source: availableTags
    });	

	$.ajax({
			url: "{{ url(config('laraadmin.adminRoute') . '/get_email_templates') }}",
			type: "GET",
			//data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(dataResult){
				var dataResult = JSON.parse(dataResult);
				if(dataResult.statusCode==200){
					document.getElementById("emailtype").innerHTML = dataResult.data;
					//$('#emailtype').append(dataResult.data);        				    
				} else {
					alert("Something Went Wrong! Please Refresh the Page");
				}        				
			}
		});		
});

function togglediv(divid){
	$("#timeline-body-"+divid).slideToggle("slow");
}	

function delitem(delid){
	//e.preventDefault();

	var fid = delid;
	//alert(fid);
	$.ajax({
				url: "{{ url(config('laraadmin.adminRoute') . '/contact_timelines/') }}" + '/' + fid,
				type: "POST",
				data:  new FormData(document.getElementById("deltimline-cont"+ fid)),
				contentType: false,
				cache: false,
				processData:false,
				success: function(dataResult){
					var dataResult = JSON.parse(dataResult);
					if(dataResult.statusCode==200){
						//alert("Deleted");
						$('#timeline-item'+ fid).addClass("delete");
						$("#deltimline-cont"+ fid).remove();
						
						//$('#emailtype').append(dataResult.data);        				    
					}else{
						alert("Something Went Wrong! Please Refresh the Page");
					}        				
				}
			});	
}

function onchangebind(e){
	//e.preventDefault();
	var fdata =  $('#tag-title').val();
		$.ajax({
				  url: "{{ url('admin/contact_timelines_add') }}",
				  type: "POST",
				  data:  new FormData(document.getElementById("contact_timeline-add-tag-form")),
				  contentType: false,
				  cache: false,
				  processData:false,
				  success: function(dataResult){
					  var dataResult = JSON.parse(dataResult);
					  if(dataResult.statusCode==200){
						$('#time-line').prepend('<li><i class="fa fa-comment bg-yellow"></i><div class="timeline-item"><span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::now()->toTimeString() }}</span><h3 class="timeline-header"><b>'+ fdata +'</b></h3></div></li>');

						$('#time-line').prepend('<li class="time-label"><span class="bg-blue">{{ \Carbon\Carbon::now()->format('d M Y') }}</span></li>');
						$('#tag-submit').val("");
					  }else{

						  //alert(dataResult.msg);
					  }
					  //setTimeout(function(){
						//$('#mssg').remove();
					  //}, 9000);        				
				  }
			  });
}	


function selemailtemp(selemail){
	if(selemail > 0){
		$.ajax({
				url: "{{ url(config('laraadmin.adminRoute') . '/get_email_template' ) }}"+'/'+selemail,
				type: "GET",
				//data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
        		success: function(dataResult){
        				var dataResult = JSON.parse(dataResult);
        				if(dataResult.statusCode==200){
							var strd = dataResult.data;
							var custn = document.getElementById("cont_name").innerHTML;
							var ebody = strd.replace("{first_name}", custn);
							document.getElementById("email_temp").innerHTML = ebody;
        				    //$('#emailtype').append(dataResult.data);        				    
							//$('#emailtype').show();
        				}else{
							alert("Something Went Wrong! Please Refresh the Page");
        				}        				
        			}
        		});
	} else {
		alert("Please Select Email template");
		document.getElementById("email_temp").innerHTML = "";
	}
}
</script>
@endpush