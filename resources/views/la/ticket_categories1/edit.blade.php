@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/ticket_categories') }}">Ticket Category</a> :
@endsection
@section("contentheader_description", $ticket_category->$view_col)
@section("section", "Ticket Categories")
@section("section_url", url(config('laraadmin.adminRoute') . '/ticket_categories'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Ticket Categories Edit : ".$ticket_category->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($ticket_category, ['route' => [config('laraadmin.adminRoute') . '.ticket_categories.update', $ticket_category->id ], 'method'=>'PUT', 'id' => 'ticket_category-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/ticket_categories') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#ticket_category-edit-form").validate({
		
	});
});
</script>
@endpush
