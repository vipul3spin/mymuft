		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Messages: style can be found in dropdown.less-->
				@if(LAConfigs::getByKey('show_messages'))
				<li class="dropdown messages-menu">
					<!-- Menu toggle button -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-envelope-o"></i>
						<span class="label label-success">4</span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">You have 4 messages</li>
						<li>
							<!-- inner menu: contains the messages -->
							<ul class="menu">
								<li><!-- start message -->
									<a href="#">
										<div class="pull-left">
											<!-- User Image -->
											<img src="@if(isset(Auth::user()->email)) {{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }} @else asset('/img/user2-160x160.jpg' @endif" class="img-circle" alt="User Image"/>
										</div>
										<!-- Message title and timestamp -->
										<h4>
											Support Team
											<small><i class="fa fa-clock-o"></i> 5 mins</small>
										</h4>
										<!-- The message -->
										<p>Why not buy a new awesome theme?</p>
									</a>
								</li><!-- end message -->
							</ul><!-- /.menu -->
						</li>
						<li class="footer"><a href="#">See All Messages</a></li>
					</ul>
				</li><!-- /.messages-menu -->
				@endif
				@if(LAConfigs::getByKey('show_notifications'))
				<!-- Notifications Menu -->
				<li class="dropdown notifications-menu">
					<!-- Menu toggle button -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-bell-o"></i>
						<span class="label label-warning">10</span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">You have 10 notifications</li>
						<li>
							<!-- Inner Menu: contains the notifications -->
							<ul class="menu">
								<li><!-- start notification -->
									<a href="#">
										<i class="fa fa-users text-aqua"></i> 5 new members joined today
									</a>
								</li><!-- end notification -->
							</ul>
						</li>
						<li class="footer"><a href="#">View all</a></li>
					</ul>
				</li>
				@endif
				@if(LAConfigs::getByKey('show_tasks'))
				<!-- Tasks Menu -->
				<li class="dropdown tasks-menu">
					<!-- Menu Toggle Button -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-flag-o"></i>
						<span class="label label-danger">9</span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">You have 9 tasks</li>
						<li>
							<!-- Inner menu: contains the tasks -->
							<ul class="menu">
								<li><!-- Task item -->
									<a href="#">
										<!-- Task title and progress text -->
										<h3>
											Design some buttons
											<small class="pull-right">20%</small>
										</h3>
										<!-- The progress bar -->
										<div class="progress xs">
											<!-- Change the css width attribute to simulate progress -->
											<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
												<span class="sr-only">20% Complete</span>
											</div>
										</div>
									</a>
								</li><!-- end task item -->
							</ul>
						</li>
						<li class="footer">
							<a href="#">View all tasks</a>
						</li>
					</ul>
				</li>
				@endif
				@if (Auth::guest())
					<li><a href="{{ url('/login') }}">Login</a></li>
					<li><a href="{{ url('/register') }}">Register</a></li>
				@else
				@if(Entrust::hasRole('SUPER_ADMIN'))
					<li class="dropdown" style="padding-top: 10px;width: 90px;">
						<?php $ausers = App\User::whereNull('deleted_at')->get(); ?>
						<select class="select2-hidden-accessible" style="border:0 px !important;" data-placeholder="Login As" rel="select2" id="switch_user" tabindex="-1" aria-hidden="true">
							<option Value="">Login As</option>
							@foreach($ausers as $user)
								<option value="{{ url(config('laraadmin.adminRoute') . '/switch/start/'.$user->id ) }}">{{ str_limit($user->name, 8) }}</option>
							@endforeach
						</select>
					</li>
				@endif
					@if(!Entrust::hasRole('LEAD_ADMIN'))
					<li class="dropdown tasks-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-money"></i> Plan
						</a> 
						<ul class="dropdown-menu" style="width:auto;">
							<li class="footer" style="text-align: center;padding: 10px;"><button class="btn btn-success btn-sm" style="background-color: #2eb912;" data-toggle="modal" data-target="#AddPlan">Add Plan</button></li>
							@if(Entrust::hasRole('SUPER_ADMIN'))
							<li class="footer"><a href="{{ url(config('laraadmin.adminRoute') . '/payments') }}">View Payments</a></li>
							@endif
						</ul>
					</li>
					@endif
					<!-- User Account Menu -->
					<li class="dropdown user user-menu">
						<!-- Menu Toggle Button -->
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<!-- The user image in the navbar-->
							<img src="{{ App\Models\Upload::getfilepath(Auth::user()->id,45) }}" class="user-image" alt="User Image"/>
							<!-- hidden-xs hides the username on small devices so only the image appears. -->
							<span class="hidden-xs">{{ Auth::user()->name }}</span>
						</a>
						<ul class="dropdown-menu">
							<!-- The user image in the menu -->
							<li class="user-header">
								<img src="{{ App\Models\Upload::getfilepath(Auth::user()->id,140) }}" class="img-circle" alt="User Image" />
								<p>
									{{ Auth::user()->name }}
									<?php
									$datec = Auth::user()['created_at'];
									?>
									<small>Member since <?php echo date("M. Y", strtotime($datec)); ?></small>
								</p>
							</li>
							<!-- Menu Body -->
							@role("SUPER_ADMIN")
							<li class="user-body">
<!--								<div class="col-xs-6 text-center mb10">
									<a href="{{ url(config('laraadmin.adminRoute') . '/lacodeeditor') }}"><i class="fa fa-code"></i> <span>Editor</span></a>
								</div>-->
								<div class="col-xs-6 text-center mb10">
									<a href="{{ url(config('laraadmin.adminRoute') . '/modules') }}"><i class="fa fa-cubes"></i> <span>Modules</span></a>
								</div>
								<div class="col-xs-6 text-center mb10">
									<a href="{{ url(config('laraadmin.adminRoute') . '/la_menus') }}"><i class="fa fa-bars"></i> <span>Menus</span></a>
								</div>
								<div class="col-xs-6 text-center mb10">
									<a href="{{ url(config('laraadmin.adminRoute') . '/la_configs') }}"><i class="fa fa-cogs"></i> <span>Configure</span></a>
								</div>
								<div class="col-xs-6 text-center">
									<a href="{{ url(config('laraadmin.adminRoute') . '/backups') }}"><i class="fa fa-hdd-o"></i> <span>Backups</span></a>
								</div>
							</li>
							@endrole
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="{{ url(config('laraadmin.adminRoute') . '/users/') .'/'. Auth::user()->id }}" class="btn btn-default btn-flat">Profile</a>
								</div>
								<div class="pull-right">
									@if(Session::has('orig_user'))
										<a href="{{ url(config('laraadmin.adminRoute') . '/switch/stop') }}" class="btn btn-default btn-flat">Logout User</a>
									@else
										<a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
									@endif
								</div>
							</li>
						</ul>
					</li>
				@endif
				@if(LAConfigs::getByKey('show_rightsidebar'))
				<!-- Control Sidebar Toggle Button -->
				<li>
					<a href="#" data-toggle="control-sidebar"><i class="fa fa-comments-o"></i> <span class="label label-warning">10</span></a>
					
				</li>
				@endif
			</ul>
		</div>

{{--@if(Entrust::hasRole('SUPER_ADMIN'))  --}}             
<div class="modal fade" id="AddPlan" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Plan</h4>
			</div>
			{!! Form::open(['url' => '/admin/purchase_plan', 'id' => 'plan-purchase-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                                @if(Entrust::hasRole('SUPER_ADMIN'))
									<div class="form-group">
										<label for="user_id">Customer* :</label>
										<select class="form-control select2-hidden-accessible" required="1" data-placeholder="Select Customer" rel="select2" name="user_id" tabindex="-1" aria-hidden="true" aria-required="true">
											<?php $custs = App\Models\Customer::all(); ?>
											@foreach($custs as $cust)
													<?php $cty = App\Models\Customer_Type::find($cust->ctype); ?>
													<option value="{{ $cust->id }}">{{ $cust->name }} - {{ $cty->name }}</option>
											@endforeach
										</select>
									</div>									
									<div class="form-group">
										<label for="role">Plan* :</label>
										<select class="form-control" id="plan" required="1" data-placeholder="Select Plan" rel="select2" name="plan">
											<?php $plas = App\Models\Pricing_Plan::all(); ?>
											@foreach($plas as $pla)
													<?php $cty = App\Models\Customer_Type::find($pla->ctype); ?>
													<option value="{{ $pla->id }}" data-pln="{{$pla->plan_fig}}" data-val="{{$pla->price}}">{{ $pla->name }} - {{ $cty->name }}</option>
											@endforeach
										</select>
									</div> 
								@elseif(Entrust::hasRole('LEAD_ADMIN'))
                                @else
									<?php 
										$usr = App\User::find(Auth::user()->id);
										$cust = App\Models\Customer::find(str_replace('c-','',$usr->link_id));
										$plas = App\Models\Pricing_Plan::where('ctype','=',$cust->ctype)->get(); 
									?>
                                    <div class="form-group"> 
                                            <input class="form-control" name="user_id" required="1" type="hidden" value="{{ $cust->id }}" aria-required="true">                                      
                                    </div> 
									<div class="form-group">
										<label for="role">Plan* :</label>
										<select class="form-control" id="plan" required="1" data-placeholder="Select Plan" rel="select2" name="plan">
											@foreach($plas as $pla)
													<option value="{{ $pla->id }}" data-pln="{{$pla->plan_fig}}" data-val="{{$pla->price}}">{{ $pla->name }} - ( INR.{{$pla->price}}/Mbps )</option>
											@endforeach
										</select>
									</div>									                               
                                @endif
                                        <div class="form-group" style="text-align: right;">
                                            <label for="amount">Amount :&nbsp;₹&nbsp;<span id="amount" style="display: inline-block;min-width: 65px;">0.0</span>&nbsp;</label><br/>
                                            <!--<label for="sramount">Service Charge ( {{ LAConfigs::getByKey('service_chrg') }}% ) :&nbsp;₹&nbsp;<span id="sramount" style="display: inline-block;min-width: 65px;">0.0</span>&nbsp;</label><br/>-->
                                            <label for="taxnamount">GST ( {{ LAConfigs::getByKey('gst') }}% ) :&nbsp;₹&nbsp;<span id="taxamount" style="display: inline-block;min-width: 65px;">0.0</span>&nbsp;</label>
                                            <hr/>
                                            <label for="txamount">Amount Payable:&nbsp;₹&nbsp;<span id="txamount" style="display: inline-block;min-width: 65px;">0.0</span>&nbsp;</label>
                                        </div>                                 
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Continue', ['class'=>'btn btn-success','style'=>'background-color: #2eb912;']) !!}
			</div>
			<!-- jQuery 3.5.1 -->
			<script src="{{ asset('la-assets/js/jQuery-3-5-1.min.js') }}"></script>
					<script>
						$(function(){
							$("#plan").change(function(){
								//alert("hi");
								var element = $('option:selected', this);
								var x = element.attr('data-pln');
								var y = element.attr('data-val');
								var amt = x*y;
								$("#amount").text(amt);
								var taxamt = amt * ({{ LAConfigs::getByKey('gst') }})/100;
								$("#taxamount").text(taxamt);
								var totamt = amt + taxamt;
								$("#txamount").text(totamt);
							});					
						});
					</script>
			{!! Form::close() !!}
		</div>
	</div>
</div>         
{{--@endif   --}}   		