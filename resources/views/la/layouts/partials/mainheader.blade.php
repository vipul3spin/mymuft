<!-- Main Header -->
<header class="main-header">
    <noscript>
    <div style="display: block;background-color: red;color: #fff;font-size:20px;text-align: center;">Enable Javascript on your browser in order to see this page in all of its glory!</div>
    </noscript>
	@if(LAConfigs::getByKey('layout') != 'layout-top-nav')
	<!-- Logo -->
	<a href="{{ url(config('laraadmin.adminRoute')) }}" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
                
		<span class="logo-mini"><img src="{{ asset('la-assets/img/muft-wifi-logo-mini.png') }}" style="max-width: 70%;mmargin-top: -3px;max-height: 50px;"/>
                    <!--<b>{{ LAConfigs::getByKey('sitename_short') }}</b>-->
                </span>
		<!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><img src="{{ asset('la-assets/img/muft-wifi-logo.png') }}" style="max-width: 70%;margin-top: -3px;max-height: 50px;"/>
                    <!--<b style="color: #00589a;">{{ LAConfigs::getByKey('sitename_part1') }}</b><b style="color: #ed6d23;">{{ LAConfigs::getByKey('sitename_part2') }}</b>-->
                </span>
	</a>
	@endif

	<!-- Header Navbar -->
	<nav class="navbar navbar-static-top" role="navigation">
	@if(LAConfigs::getByKey('layout') == 'layout-top-nav')
		<div class="container">
			<div class="navbar-header">
				<a href="{{ url(config('laraadmin.adminRoute')) }}" class="navbar-brand">
					<b style="color: #00589a;">{{ LAConfigs::getByKey('sitename_part1') }}</b> <b style="color: #ed6d23;">{{ LAConfigs::getByKey('sitename_part2') }}</b>
				</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>
			@include('la.layouts.partials.top_nav_menu')
			@include('la.layouts.partials.notifs')
		</div><!-- /.container-fluid -->
	@else
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle b-l" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		@include('la.layouts.partials.notifs')
	@endif
	
	</nav>
</header>
