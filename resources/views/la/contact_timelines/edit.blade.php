@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/contact_timelines') }}">Contact Timeline</a> :
@endsection
@section("contentheader_description", $contact_timeline->$view_col)
@section("section", "Contact Timelines")
@section("section_url", url(config('laraadmin.adminRoute') . '/contact_timelines'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Contact Timelines Edit : ".$contact_timeline->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($contact_timeline, ['route' => [config('laraadmin.adminRoute') . '.contact_timelines.update', $contact_timeline->id ], 'method'=>'PUT', 'id' => 'contact_timeline-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'cont_id')
					@la_input($module, 'type')
					@la_input($module, 'title')
					@la_input($module, 'description')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/contact_timelines') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#contact_timeline-edit-form").validate({
		
	});
});
</script>
@endpush
