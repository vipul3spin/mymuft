@extends("la.layouts.app")

@section("contentheader_title")
    Ticket : | <a href="{{ url(config('laraadmin.adminRoute') . '/tickets/'.$ticket->ticket_id.'/edit') }}">#{{ $ticket->ticket_id }}</a> |
@endsection
@section("contentheader_description",'Edit')
@section("section", "Tickets")
@section("section_url", url(config('laraadmin.adminRoute') . '/tickets'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Tickets Edit : #".$ticket->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-info">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($ticket, ['route' => [config('laraadmin.adminRoute') . '.tickets.update', $ticket->ticket_id ], 'method'=>'PUT', 'id' => 'ticket-edit-form']) !!}
                                        @la_input($module, 'cust_id')
                                        @la_input($module, 'title')
					@la_input($module, 'description')
					@la_input($module, 'cat_id')
					@la_input($module, 'priority')
					@la_input($module, 'status')
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-info']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/tickets') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#ticket-edit-form").validate({
		
	});
});
</script>
@endpush
