@extends("la.layouts.app")

@section("htmlheader_title")Support Ticket View @endsection
@section("contentheader_title")
    Ticket : | <a href="{{ url(config('laraadmin.adminRoute') . '/tickets/'.$ticket->ticket_id) }}">#{{ $ticket->ticket_id }}</a> |
@endsection
@section("contentheader_description") view @endsection
@section("section", "Ticket")
@section("section_url", url(config('laraadmin.adminRoute') . '/tickets'))
@section("sub_section", "View")

@section("headerElems")
@la_access("Tickets", "edit")
	<!--<button class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Raise a Ticket</button>-->
                        @if($ticket->status <> "Closed")
                        {!! Form::open(['action' => 'LA\TicketsController@close', 'id' => 'close-ticket-form', 'style' => 'display: inline-block;']) !!}
                            <input type="hidden" name="ticket_id" value="{{ $ticket->ticket_id }}">
                            <button type="submit" class="btn btn-sm pull-right btn-edit btn-danger"><i class="fa fa-close"></i>&nbsp;&nbsp;Close</button>
                        {!! Form::close() !!}
                        @endif
@endla_access
@endsection

@section("main-content")
    <div class="row">
        <section class="col-lg-12">
            <div class="box box-info">
                <div class="box-header">     
                    <p class="box-title"><b>Title :</b>&nbsp;&nbsp;{{ $ticket->title }}</p>
                </div>
            </div>
        </section>        
    </div>
    <div class="row text-left">
        <section class="col-lg-4">
            <div class="box box-info">
                <div class="box-body">
                    <p class="col-xs-5"><b><i class="fa fa-ticket"></i>&nbsp;&nbsp;Ticket ID :</b></p><p class="col-xs-7">#{{ $ticket->ticket_id }}</p>
                    <p class="col-xs-5"><b><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Created on :</b></p><p class="col-xs-7">{{ $ticket->created_at }}</p>
                    <p class="col-xs-5"><b><i class="fa fa-certificate"></i>&nbsp;&nbsp;Category :</b></p><p class="col-xs-7">{{ App\Models\Ticket_Category::where('id','=',$ticket->cat_id)->value('name') }}</p>
                    <p class="col-xs-5"><b><i class="fa fa-star"></i>&nbsp;&nbsp;Priority :</b></p><p class="col-xs-7">{{ $ticket->priority }}</p>                    
                    <p class="col-xs-5"><b><i class="fa fa-flag"></i>&nbsp;&nbsp;Status :</b></p><p class="col-xs-7"> 
                        @if ($ticket->status === 'Open')
                            <span class="label label-success">{{ $ticket->status }}</span>
                        @elseif ($ticket->status === 'On Hold')
                            <span class="label label-warning">{{ $ticket->status }}</span>
                        @else
                            <span class="label label-danger">{{ $ticket->status }}</span>
                        @endif
                    </p>
                </div> 
            </div>            
        </section>
        <section class="col-lg-8">
            <div class="box box-info">
                <div class="box-header">
                    <div class="col-xs-5">
                        <b>Description :</b>
                    </div>
                    <div class="col-xs-7">
                    <div class="pull-right">
                        <a href="{{ url(config('laraadmin.adminRoute') . '/tickets/'.$ticket->ticket_id.'/edit') }}" class="btn btn-xs btn-edit btn-info" style="display: inline-block;"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</a>
                    </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-lg-12">
                        {{ $ticket->description }}                       
                    </div>  
                </div>                
            </div>
        </section>
    </div>
    <div class="row">
    <section class="col-lg-12">
      <!-- Chat box -->
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-comments-o"></i>
          <h3 class="box-title"><b>Comments</b></h3>
        </div>
	<hr style="margin-top:0;" />
        <div class="box-body chat" id="chat-box">
           @if($comments == null)
                <p>Comments not found.</p>
           @else
	   <?php $mcount = count($comments); $cm = 1 ; ?>
           @foreach ($comments as $comment)
          <!-- chat item -->
          <div class="item">
            <img src="{{ App\Models\Upload::getfilepath($comment->cust_id,45) }}" alt="user image">
            <p class="message">
              <a href="" class="name">
                <small class="text-muted pull-right">{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }} <i class="fa fa-clock-o"></i></small>
                <?php
                $role_id = App\Models\Customer::where('id','=',$comment->cust_id)->value('role');
                $role = App\Role::where('id','=',$role_id)->value('name');
                ?>
                @if($role == "SUPER_ADMIN")
                    Support Admin
                @else
                    {{ App\User::find($comment->cust_id)->name }}
                @endif
              </a>
              {{ $comment->comment }}
            </p>
            @if(!empty($comment->attach))
                <?php $upload = App\Models\Upload::find($comment->attach); ?>
                    <div class="attachment">
                      <h4>Attachment : &nbsp;&nbsp; {{ $upload->name }}</h4>
                      <p class="filename">
                          <a target="_blank" href="{{ url("files/".$upload->hash.DIRECTORY_SEPARATOR.$upload->name) }}"><image src="{{ url("files/".$upload->hash.DIRECTORY_SEPARATOR.$upload->name."?s=80") }}"/></a>
                      </p>
<!--                      <div class="pull-right">
                          <a target="_blank" href="{{ url("files/".$upload->hash.DIRECTORY_SEPARATOR.$upload->name) }}"><button class="btn btn-primary btn-sm btn-flat">Open</button></a>
                      </div>-->
                    </div><!-- /.attachment -->            
            @endif
          </div><!-- /.item -->
	@if($cm < $mcount)
		<?php $cm = $cm + 1; ?>
		<hr />		
	@endif
            @endforeach
          @endif
        </div><!-- /.chat -->
        @if($ticket->status <> "Closed")
	<hr  />
	<h4 style="margin-left: 10px;"><i class="fa fa-comments-o"></i>&nbsp;&nbsp;<b>Write a comment</b></h4>
        <div class="box-footer">
            {!! Form::open(['action' => 'LA\CommentsController@store', 'id' => 'comment-add-form']) !!}

            <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
            <input type="hidden" name="cust_id" value="{{ Auth::User()->id }}">
            <div class="form-group">
                <label for="message">Message :</label>
                <textarea class="form-control" name="comment" placeholder="Type a message..."></textarea>                
            </div>
@la_input($module1,'attach')
            <div class="form-group">
                <div class="form-group-btn">
                  <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}    
        </div>
        @endif
      </div><!-- /.box (chat box) -->            
    </section>
    </div>
@endsection