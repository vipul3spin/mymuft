@extends("la.layouts.app")

@section("contentheader_title", "Import")
@section("contentheader_description", "Import")
@section("section", "Import")
@section("sub_section", "Import")
@section("htmlheader_title", "Import")

@section("headerElems")
@la_access("Employees", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#importfrm">Excel Import</button>
@endla_access
@endsection

@section("main-content")


<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		@if(isset($datas) || $datas <> "" || $datas <> NULL)
			GSTIN: {{ $datas['gstin'] }}
			<br/>
			FP: {{ $datas['fp'] }}
			<br/>
			Filing Type: {{ $datas['filing_typ'] }}
			<br/>
			GT: {{ $datas['gt'] }}
			<br/>
			Current GT: {{ $datas['cur_gt'] }}
			<br/>
			File Date: {{ $datas['fil_dt'] }}
		@endif 

	</div>
</div>


@la_access("Employees", "create")
  <div class="modal fade" id="importfrm" role="dialog" aria-labelledby="myModalLabel">
  	<div class="modal-dialog" role="document">
  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  				<h4 class="modal-title" id="myModalLabel">Excel Import</h4>
  			</div>
  			{!! Form::open(['action' => 'LA\ImportController@importCsv', 'id' => 'users_profile-add-form', 'enctype' => 'multipart/form-data']) !!}
  			<div class="modal-body">
  				<div class="box-body">
  					<div class="form-group">
  						<label for="inputfile">File:</label>
              <input type="file" class="form-control" required="1" data-placeholder="Select File" rel="import_file" name="import_file">
  					</div>
  				</div>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
  			</div>
  			{!! Form::close() !!}
  		</div>
  	</div>
  </div>
  @endla_access

<?php
function array2table($array, $recursive = false, $null = '&nbsp;')
		{
		    // Sanity check
		    if (empty($array) || !is_array($array)) {
		        return false;
		    }

		    if (!isset($array[0]) || !is_array($array[0])) {
		        $array = array($array);
		    }

		    // Start the table
		    $table = "<table id='example1' class='table table-bordered'>\n";

		    // The header
		    $table .= "\t<tr class='success'>";
		    // Take the keys from the first row as the headings
		    foreach (array_keys($array[0]) as $heading) {
		        $table .= '<th>' . $heading . '</th>';
		    }
		    $table .= "</tr>\n";

		    // The body
		    foreach ($array as $row) {
		        $table .= "\t<tr>" ;
		        foreach ($row as $cell) {
		            $table .= '<td>';

		            // Cast objects
		            if (is_object($cell)) { $cell = (array) $cell; }

		            if ($recursive === true && is_array($cell) && !empty($cell)) {
		                // Recursive mode
		                $table .= "\n" . array2table($cell, true, true) . "\n";
		            } else {
		                $table .= (strlen($cell) > 0) ?
		                    htmlspecialchars((string) $cell) :
		                    $null;
		            }

		            $table .= '</td>';
		        }

		        $table .= "</tr>\n";
		    }

		    $table .= '</table>';
		    return $table;
		}

?>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
@endpush
