@extends("la.layouts.app")

@section("contentheader_title", "Muft WiFi")
@section("contentheader_description", "Order Payment")
@section("section", "Order")
@section("sub_section", "Payment")
@section("htmlheader_title", "Order Details")

@section('main-content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- Main content -->
<div class="box box-info">
	<!--<div class="box-header"></div>-->
    <div class="box-body col-lg-offset-1">
        <div class="col-md-6">
            <div class="panel-heading"><h3>Order Details</h3></div>
            <div class="box box-info" style="background-color: #ecf0f5;padding: 20px;">
                <table class="table table-bordered">
                <tr>
                    <td><div class="col-md-6">Order ID :</div><div class="col-md-6">{{$payment['order_id']}}</div></td>
                </tr>
                <tr>
                    <td><div class="col-md-6">Plan Price :</div><div class="col-md-6">₹&nbsp;{{$payment['amt']}}</div></td>
                </tr>
                <tr style="display:none;">
                    <td><div class="col-md-6">Service Charge({{$payment['sertax_par']}}%) :</div><div class="col-md-6">₹&nbsp;{{$payment['sramt']}}</div></td>
                </tr>
                <tr>
                    <td><div class="col-md-6">GST({{$payment['tax_par']}}%) :</div><div class="col-md-6">₹&nbsp;{{$payment['taxamt']}}</div></td>
                </tr>
                <tr>
                    <td><div class="col-md-6">Total Amount Payable :</div><div class="col-md-6">₹&nbsp;{{$payment['txnamt']}}</div></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-6 text-center">
                            <button onClick="canceldialog(); return false;" class="btn btn-danger">Cancel</button>
                        </div>
                        <div class="col-md-6 text-center">
                            {!! Form::open(['action' => 'LA\RazorpayController@payment','id' => 'orderform']) !!}
                            <!-- Note that the amount is in paise = 50 INR -->
                            <!--amount need to be in paisa-->  
                            <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" value="">
                            <button id="rzp-button1" class="btn btn-success" style="background-color: #2eb912;">Pay Now</button>
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
                </table>
            </div>      
        </div>
    </div>
</div>
<!-- order cancel form -->
{!! Form::open(['action' => 'LA\RazorpayController@payment','id' => 'ordercancel']) !!}
<input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" value="null">
<input type="hidden" name="mworder_id" id="mworder_id" value="{{$payment['order_id']}}">
{!! Form::close() !!}

<script src="{{ asset('la-assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('la-assets/plugins/sweetalert2/sweetalert2.min.css') }}">
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
var options =<?php echo $data; ?>;
options.handler = function (response){
    if (typeof response.razorpay_payment_id == 'undefined' ||  response.razorpay_payment_id < 1) {
        canceldialog();
    } else {
        //alert(response.razorpay_payment_id);
        document.getElementById("razorpay_payment_id").value = response.razorpay_payment_id;
        swal({
            type: 'success',
            title: 'Transaction Successful',
            text: 'We will redirect you shortly.',
            showConfirmButton: false
            });
        document.getElementById("orderform").submit();
        //alert("Order Sucessful");
    }
};

options.modal = {
    ondismiss: function() {
        canceldialog();
    },
    escape: false,
    backdropclose: false
};

var rzp1 = new Razorpay(options);

document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}
function canceldialog(){
    swal({
        title: 'Are you sure?',
        text: "Do you want to cancel the Transaction?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#2eb912',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          swal({
                title :'Transaction Unsuccessful',
                text: 'We will redirect you shortly.',
                type: 'error',
                showConfirmButton: false
                });
            document.getElementById("ordercancel").submit();
        } else {
            rzp1.open();
        }
      });    
}
</script>
@endsection