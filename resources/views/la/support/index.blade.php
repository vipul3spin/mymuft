@extends("la.layouts.app")

@section("contentheader_title", "Support")
@section("contentheader_description", "Need help?")
@section("section", "Support")
@section("sub_section", "")
@section("htmlheader_title", "Support")

@section("headerElems")
@la_access("Tickets", "create")
	<!--<button class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Raise a Ticket</button>-->
@endla_access
@endsection

@section("main-content")

          <div class="row">
            <div class="col-md-3">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
		@if(Entrust::hasRole('SUPER_ADMIN'))
                  <h3>{{ App\Models\Ticket::gettotal() }}</h3>
                  <p>Total Tickets</p>                  
		@else
		  <h3>{{ App\Models\Ticket::gettotal(Auth::user()->id) }}</h3>
                  <p>My Tickets</p>                  
		@endif
                </div>
                <div class="icon">
                  <i class="fa fa-ticket"></i>
                </div>
                <a href="{{ url(config('laraadmin.adminRoute')) }}/tickets" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-md-3">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
		@if(Entrust::hasRole('SUPER_ADMIN'))
                  <h3>{{ App\Models\Ticket::getcount('Closed') }}</h3>
		@else
		  <h3>{{ App\Models\Ticket::getcount('Closed',Auth::user()->id) }}</h3>
		@endif
                  <p>Resolved</p>
                </div>
                <div class="icon">
                  <i class="fa fa-check-circle-o"></i>
                </div>
                <a href="{{ url(config('laraadmin.adminRoute')) }}/tickets" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>                  
              </div>
            </div><!-- ./col -->
            <div class="col-md-3">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
		@if(Entrust::hasRole('SUPER_ADMIN'))
                  <h3>{{ App\Models\Ticket::getcount('Open') }}</h3>
		@else
		  <h3>{{ App\Models\Ticket::getcount('Open',Auth::user()->id) }}</h3>
		@endif
                  <p>Open</p>
                </div>
                <div class="icon">
                  <i class="fa fa-file-o"></i>
                </div>
                <a href="{{ url(config('laraadmin.adminRoute')) }}/tickets" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>                  
              </div>
            </div><!-- ./col -->
            <div class="col-md-3">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
		@if(Entrust::hasRole('SUPER_ADMIN'))
                  <h3>{{ App\Models\Ticket::getcount('On Hold') }}</h3>
		@else
		  <h3>{{ App\Models\Ticket::getcount('On Hold',Auth::user()->id) }}</h3>
		@endif
                  <p>On Hold</p>
                </div>
                <div class="icon">
                  <i class="fa fa-clock-o"></i>
                </div>
                <a href="{{ url(config('laraadmin.adminRoute')) }}/tickets" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>                  
              </div>
            </div><!-- ./col -->            
          </div><!-- /.row -->

<!-- Main content -->
    <div class="row">
            <div class="col-md-6">
                  <div class="box box-info text-center" style="padding: 20px;">
                      <h2>Read our FAQs?</h2>
                      <h3>See if your question has already been asked and answered.</h3><br/>
                    <a href="http://support.muftwifi.com" target="_blank"><button class="btn btn-info">Read Now</button></a>
                    <br/><br/>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="box box-info text-center" style="padding: 20px;">
                      <h2>Need More Help?</h2>
                      <h3>Post to ask questions to Muft WiFi community members.</h3><br/>
		      <button class="btn btn-info" data-toggle="modal" data-target="#AddModal">Raise a Ticket</button>
                    <!--<a href="http://muftwifi.com/software/post-your-query/" target="_blank"><button class="btn btn-info">Click Here</button></a>-->
                    <br/><br/>
                  </div>
              </div>
    </div>

@la_access("Tickets", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">New Ticket</h4>
			</div>
			{!! Form::open(['action' => 'LA\TicketsController@store', 'id' => 'ticket-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                                    @if(Entrust::hasRole('SUPER_ADMIN'))
                                        @la_input($module, 'cust_id')
                                    @endif
					@la_input($module, 'title')
					@la_input($module, 'cat_id')                                        
					@la_input($module, 'description')					

					@la_input($module, 'priority')					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access
<!-- /.content -->
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush