@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/orders') }}">Order</a> :
@endsection
@section("contentheader_description", $order->$view_col)
@section("section", "Orders")
@section("section_url", url(config('laraadmin.adminRoute') . '/orders'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Orders Edit : ".$order->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($order, ['route' => [config('laraadmin.adminRoute') . '.orders.update', $order->id ], 'method'=>'PUT', 'id' => 'order-edit-form']) !!}
					{{-- @la_form($module) --}}
					
					@la_input($module, 'order_id')
					@la_input($module, 'choices')
					@la_input($module, 'name')
					@la_input($module, 'cemail')
					@la_input($module, 'mmobno')
					@la_input($module, 'pann')
					{{-- @la_input($module, 'panp')
					@la_input($module, 'idp') --}}
					@la_input($module, 'bandw')
					@la_input($module, 'loca')
					@la_input($module, 'staddr')
					@la_input($module, 'addr')
					@la_input($module, 'zipn')
					@la_input($module, 'state')
					@la_input($module, 'city')
					@la_input($module, 'gstn')
					<div class="form-group">
						<label class="form-label">Status</label>
						<select name="status" class="form-control">
							<option value="Pending">Pending</option>
							<option value="Approved">Approved</option>
							<option value="Cancle">Cacled</option>
							<option value="Fack">Fack</option>
						</select>
						<input type="hidden" name="updated_by" value="{{ Auth::User()->id }}">
					</div>

					{{-- @la_input($module, 'status')
					@la_input($module, 'updated_by')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/orders') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#order-edit-form").validate({
		
	});
});
</script>
@endpush
