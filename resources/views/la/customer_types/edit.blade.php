@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/customer_types') }}">Customer Type</a> :
@endsection
@section("contentheader_description", $customer_type->$view_col)
@section("section", "Customer Types")
@section("section_url", url(config('laraadmin.adminRoute') . '/customer_types'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Customer Types Edit : ".$customer_type->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($customer_type, ['route' => [config('laraadmin.adminRoute') . '.customer_types.update', $customer_type->id ], 'method'=>'PUT', 'id' => 'customer_type-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/customer_types') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#customer_type-edit-form").validate({
		
	});
});
</script>
@endpush
