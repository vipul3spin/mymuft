@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/payments') }}">Payment</a> :
@endsection
@section("contentheader_description", $payment->$view_col)
@section("section", "Payments")
@section("section_url", url(config('laraadmin.adminRoute') . '/payments'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Payments Edit : ".$payment->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($payment, ['route' => [config('laraadmin.adminRoute') . '.payments.update', $payment->id ], 'method'=>'PUT', 'id' => 'payment-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'order_id')
					@la_input($module, 'user_id')
					@la_input($module, 'plan')
					@la_input($module, 'amt')
					@la_input($module, 'sertax_par')
					@la_input($module, 'sramt')
					@la_input($module, 'tax_par')
					@la_input($module, 'taxamt')
					@la_input($module, 'txnamt')
					@la_input($module, 'txn_date')
					@la_input($module, 'm_id')
					@la_input($module, 'pay_id')
					@la_input($module, 'attempts')
					@la_input($module, 'currency')
					@la_input($module, 'method')
					@la_input($module, 'status')
					@la_input($module, 'pay_order_id')
					@la_input($module, 'pay_invoice_id')
					@la_input($module, 'international')
					@la_input($module, 'description')
					@la_input($module, 'card_id')
					@la_input($module, 'bank')
					@la_input($module, 'wallet')
					@la_input($module, 'vpa')
					@la_input($module, 'email')
					@la_input($module, 'contact')
					@la_input($module, 'fee')
					@la_input($module, 'tax')
					@la_input($module, 'error_code')
					@la_input($module, 'error_description')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/payments') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#payment-edit-form").validate({
		
	});
});
</script>
@endpush
