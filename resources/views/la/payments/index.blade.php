@extends("la.layouts.app")

@section("contentheader_title", "Payments")
@section("contentheader_description", "Payments listing")
@section("section", "Payments")
@section("sub_section", "Listing")
@section("htmlheader_title", "Payments Listing")

@section("headerElems")
@la_access("Payments", "create")
	<!-- <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Payment</button> -->
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Payments", "")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Payment</h4>
			</div>
			{!! Form::open(['action' => 'LA\PaymentsController@store', 'id' => 'payment-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    {{-- @la_form($module) --}}
					
					{{--
					@la_input($module, 'order_id')
					@la_input($module, 'user_id')
					@la_input($module, 'plan')
					@la_input($module, 'amt')
					@la_input($module, 'sertax_par')
					@la_input($module, 'sramt')
					@la_input($module, 'tax_par')
					@la_input($module, 'taxamt')
					@la_input($module, 'txnamt')
					@la_input($module, 'txn_date')
					@la_input($module, 'm_id')
					@la_input($module, 'pay_id')
					@la_input($module, 'attempts')
					@la_input($module, 'currency')
					@la_input($module, 'method')
					@la_input($module, 'status')
					@la_input($module, 'pay_order_id')
					@la_input($module, 'pay_invoice_id')
					@la_input($module, 'international')
					@la_input($module, 'description')
					@la_input($module, 'card_id')
					@la_input($module, 'bank')
					@la_input($module, 'wallet')
					@la_input($module, 'vpa')
					@la_input($module, 'email')
					@la_input($module, 'contact')
					@la_input($module, 'fee')
					@la_input($module, 'tax')
					@la_input($module, 'error_code')
					@la_input($module, 'error_description')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/payment_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#payment-add-form").validate({
		
	});
});
</script>
@endpush
