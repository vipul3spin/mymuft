@extends('la.layouts.app')

@section('htmlheader_title')
	Payment Details
@endsection

@section('main-content')
<div id="page-content" class="profile2">
	<div class="bg-primary clearfix">
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-3">
					<div class="profile-icon text-primary"><i class="fa fa-money"></i></div>
				</div>
				<div class="col-md-9">
					<h4 class="name">Payment</h4>
					<div class="row stats">
                                            <div class="col-md-12"><b>Order ID:</b> {{$payments->order_id}}</div>
                                            <div class="col-md-12"><b>DateTime:</b> {{$payments->created_at}}</div>
					</div>
                                                                              
				</div>
			</div>
		</div>
            <div class="col-md-5">
                <div class="dats1">
                    <p class="desc">
                        @if($payments->status == "captured")
                            Payment Success
                        @endif
                        @if($payments->status == "failed")
                            Payment Failed. Please check with the custmer.
                        @endif 
                        @if($payments->status == "created" || $payments->status == "authorized")
                            Payment is under process. Please check back after sometime.
                        @endif
                    </p>
                </div>
            </div>
		<div class="col-md-2">
                    @if($payments->status == "captured")
                        <div class="profile-icon text-green"><i class="fa fa-check"></i></div>
                    @elseif($payments->status == "failed")
                        <div class="profile-icon text-red"><i class="fa fa-close"></i></div>
                    @else
                        <div class="profile-icon text-yellow"><i class="fa fa-exclamation-circle"></i></div>
                    @endif
		</div>
		<div class="col-md-1 actions">
<!--			@la_access("Payments", "delete")
                            <a href="" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br>
			@endla_access
			
			@la_access("Payments", "delete")
                            <button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
			@endla_access-->
		</div>
            
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
            <li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/payments') }}" data-toggle="tooltip" data-placement="right" title="Back to Payments"><i class="fa fa-chevron-left"></i></a></li>
            <li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-order-info" data-target="#tab-info"><i class="fa fa-bars"></i> Order Details</a></li>
            <li class=""><a role="tab" data-toggle="tab" href="#tab-pginfo" data-target="#tab-pginfo"><i class="fa fa-money"></i> Payment Details</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Order Details</h4>
					</div>
					<div class="panel-body">
                                           <div class ="col-md-6"> 
                                            <div class="form-group col-md-12">
                                                <label class="col-md-5">ID</label>
                                                <div class="col-md-7 fvalue">{{$payments->id}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-5">User</label>
                                                <div class="col-md-7 fvalue"><a href="{{ url(config('laraadmin.adminRoute') . '/customers/'.$payments->user_id) }}" >{{ $payments->user_name }}</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-5">Order ID</label>
                                                <div class="col-md-7 fvalue">{{$payments->order_id}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-5">DateTime</label>
                                                <div class="col-md-7 fvalue">{{$payments->created_at}}
                                                </div>
                                            </div>
                                               <div class="form-group"></div>
                                           </div>
                                           <div class="col-md-6">
                                            <div class="form-group col-md-12">
                                                <label class="col-md-5">Credit Purchased</label>
                                                <div class="col-md-7 fvalue">{{$payments->buy_credits}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-5">Amount</label>
                                                <div class="col-md-7 fvalue">{{$payments->amt}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-5">Service Charge ( {{ $payments->sertax_par }}% )</label>
                                                <div class="col-md-7 fvalue">{{$payments->sramt}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-5">GST ( {{ $payments->tax_par }}% )</label>
                                                <div class="col-md-7 fvalue">{{$payments->taxamt}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="col-md-5">Total Amount Paid</label>
                                                <div class="col-md-7 fvalue">{{$payments->txnamt}}
                                                </div>
                                            </div>
                                               <div class="form-group"></div>
                                           </div>
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade in" id="tab-pginfo">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Payment Details</h4>
					</div>
					<div class="panel-body">
                                            <div class="form-group col-md-6" style="margin: 0px;">
                                                <label class="col-md-5">Order ID</label>
                                                <div class="col-md-7 fvalue">
                                                        {{$payments->order_id}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Transaction Amount</label>
                                                <div class="col-md-7 fvalue">
                                                        {{$payments->txnamt}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Currency</label>
                                                <div class="col-md-7 fvalue">{{$payments->currency}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Payment ID</label>
                                                <div class="col-md-7 fvalue">{{$payments->pay_id}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Payment Order ID</label>
                                                <div class="col-md-7 fvalue">{{$payments->pay_order_id}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Attempts</label>
                                                <div class="col-md-7 fvalue">{{$payments->attempts}}
                                                </div>
                                            </div>                                             
<!--                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Status</label>
                                                <div class="col-md-7 fvalue">
                                                    @if($payments->status == "captured")
                                                        <span class='status-label label label-success'>Success</span>
                                                    @elseif($payments->status == "failed")
                                                        <span class='status-label label label-danger'>Failed</span>
                                                    @else
                                                        <span class='status-label label label-warning'>Pending</span>
                                                    @endif
                                                </div>
                                            </div>-->
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">International Card</label>
                                                <div class="col-md-7 fvalue">{{$payments->international}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Description</label>
                                                <div class="col-md-7 fvalue">{{$payments->description}}
                                                </div>
                                            </div>                                            
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Gateway Name</label>
                                                <div class="col-md-7 fvalue">{{$payments->method}}
                                                </div>
                                            </div>
                                            @if($payment->method == 'card')
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-5">Card Number/ID</label>
                                                    <div class="col-md-7 fvalue">{{$payments->card_id}}
                                                    </div>
                                                </div>
                                            @elseif($payment->method == 'netbanking')
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-5">Bank Name</label>
                                                    <div class="col-md-7 fvalue">{{$payments->bank}}
                                                    </div>
                                                </div>
                                            @elseif($payment->method == 'wallet')
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-5">Wallet Name</label>
                                                    <div class="col-md-7 fvalue">{{$payments->wallet}}
                                                    </div>
                                                </div>
                                            @elseif($payment->method == 'upi')
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-5">UPI Address</label>
                                                    <div class="col-md-7 fvalue">{{$payments->vpa}}
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Customer Email</label>
                                                <div class="col-md-7 fvalue">{{$payments->email}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Contact</label>
                                                <div class="col-md-7 fvalue">{{$payments->contact}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Fee (Inc. GST)</label>
                                                <div class="col-md-7 fvalue">{{$payments->fee}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">GST Amount</label>
                                                <div class="col-md-7 fvalue">{{$payments->tax}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Error Code</label>
                                                <div class="col-md-7 fvalue">{{$payments->error_code}}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-md-5">Error Description</label>
                                                <div class="col-md-7 fvalue">{{$payments->error_description}}
                                                </div>
                                            </div>
					</div>
				</div>
			</div>
		</div>            
	</div>
    
</div>
@endsection