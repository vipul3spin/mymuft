<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ LAConfigs::getByKey('site_description') }}">
    <meta name="author" content="Muft Internet" />
    <meta property="og:title" content="{{ LAConfigs::getByKey('sitename') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="{{ LAConfigs::getByKey('site_description') }}" />
     
    <title>@hasSection('htmlheader_title')@yield('htmlheader_title') - @endif{{ LAConfigs::getByKey('sitename') }}</title>
    
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/la-assets/css/bootstrap.css') }}" rel="stylesheet">

	<link href="{{ asset('la-assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- Custom styles for this template -->
    <link href="{{ asset('/la-assets/css/main.css') }}" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

    <script src="{{ asset('/la-assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('/la-assets/js/smoothscroll.js') }}"></script>
    
<style>
#loading-overlay {
  position: fixed; /* Sit on top of the page content */
  display: none; /* Hidden by default */
  width: 100%; /* Full width (cover the whole page) */
  height: 100%; /* Full height (cover the whole page) */
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5); /* Black background with opacity */
  z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
  cursor: pointer; /* Add a pointer on hover */
}
#loading-overlay img{    
    position: fixed;
    top: 50%;
    left: 50%;
    right: 50%;
    bottom: 50%;
    z-index: 99999;
}
</style>
@stack('styles')
</head>

<body data-spy="scroll" data-offset="0" data-target="#navigation">
<div id="loading-overlay"><img src="{{ url('/la-assets/img/loading.gif') }}"/></div>
<!-- Fixed navbar -->
<div id="navigation" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div  style="width: 100%;">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" style="display:none;">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="padding:0px;"><img src="{{ asset('la-assets/img/muft-internet-logo.png') }}" style="max-height: 50px;"/></a>
                <select id="lang" name="lang" onchange="changelang(this.value)" class="form-control" style="float:right;margin-top: 10px;margin-bottom: 10px;width: auto;">
                    <option value="en">English</option>
                    <option value="hi">हिन्दी</option>
                </select>   
        </div>
        <div class="navbar-collapse collapse">
            <!-- <ul class="nav navbar-nav">
                <li class="active"><a href="#home" class="smoothScroll">Home</a></li>
                <li><a href="#about" class="smoothScroll">About</a></li>
                <li><a href="#contact" class="smoothScroll">Contact</a></li>
            </ul> -->
            
            <ul class="nav navbar-nav navbar-right" style="display:none !important;">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <!--<li><a href="{{ url('/register') }}">Register</a></li>-->
                @else
                    <li><a href="{{ url(config('laraadmin.adminRoute')) }}">{{ Auth::user()->name }}</a></li>
                @endif             
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<section id="contact" name="contact"></section>
<div>
    <div class="container">
        @yield('main-content')
    </div>
</div>
<div id="c" style="display:none;">
    <div class="container">
        <p>
            <strong>Copyright &copy; 2020. Powered by <a href="#"><b>Muft Internet</b></a>
        </p>
    </div>
</div>

@stack('scripts')
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/la-assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- <script src="https://www.google.com/recaptcha/api.js?render=6LeqplsaAAAAAH-4lajVewFcyxURRuw2cgOHrj-6"></script> -->
</body>
</html>
