<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order Details</title>
</head>
<body>
    <div dir="ltr">
        <p style="font-size:12.8px;margin-bottom:0.0001pt">
            <span style="font-size:small">Dear {{ $data->name }},</span>
            <br style="font-size:small">
            <br style="font-size:small">
            <span style="font-size:small">Thank you for submitting your purchase request for&nbsp;Bandwidth via Muft Internet!</span>
            <br style="font-size:small">
                Your order number is: [ {{ $data->order_id }} ]
        </p>
        <p style="font-size:12.8px;margin-bottom:0.0001pt">Please use this order number for further communication.
        </p>
        <p style="margin-bottom:0.0001pt">
            <span style="font-size:small">Someone within our network of Class A ISPs shall evaluate your query and contact you within 24 Hours.</span>
            <br style="font-size:small">
            <br style="font-size:small">
            <span style="font-size:small">If your query is missed out, feel free to send us an email on info [at]&nbsp;</span>
            <a href="http://muftinternet.com/" style="font-size:small" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://muftinternet.com/&amp;source=gmail&amp;ust=1619843484610000&amp;usg=AFQjCNHpCcc2tgvEeNfSJ1eu3Lv2-QpVXw">muftinternet.com</a>&nbsp;
            <br style="font-size:small">
            <br style="font-size:small">
            <span style="font-size:small">Best Regards,</span>
            <br style="font-size:small">
            <br style="font-size:small">
            <span style="font-size:small">Harsh Lodha</span>
            <br style="font-size:small">
            <span style="font-size:small">ISP Consultant, Muft Internet</span>
            <br style="font-size:small"><a href="http://www.muftinternet.com/" style="font-size:small" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.muftinternet.com/&amp;source=gmail&amp;ust=1619843484610000&amp;usg=AFQjCNFiDOtnLyMpgESrn2hoNfazhdz0zw">www.muftinternet.com</a>
            <br style="font-size:small">
            <span style="font-size:small">+91 81699 33736</span>
        </p>
    </div>
</body>
</html>