<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>New Order received for {{ $data->choices }}</title>
</head>
<body>

Hi,

New order for {{ $data->choices }} is received.

<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" style="font-family:sans-serif;font-size:12px;max-width: 550px;">
    <tbody>
        <tr bgcolor="#EAF2FA">
            <td><strong>Order Number</strong></td>
            <td>{{ $data->order_id }}</td>
        </tr>    
        <tr bgcolor="#FFFFFF">
            <td><strong>Order for</strong></td>
            <td>{{ $data->choices }}</td>
        </tr>
        <tr bgcolor="#EAF2FA">
            <td><strong>Name</strong></td>
            <td>{{ $data->name }}</td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td><strong>Company Email</strong></td>
            <td><a href="mailto:{{ $data->cemail }}" target="_blank">{{ $data->cemail }}</a></td>
        </tr>
        <tr bgcolor="#EAF2FA">
            <td><strong>Contact Number</strong></td>
            <td>{{ $data->mmobno }}</td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td><strong>PAN No.</strong></td>
            <td>{{ $data->pann }}</td>
        </tr>
        <tr bgcolor="#EAF2FA">
            <td><strong>PAN Card Photocopy</strong></td>
            <td><a href="{{ url('docs/'.$data->panp) }}" title="Click to view" target="_blank" data-saferedirecturl="{{ url('docs/'.$data->panp) }}">{{ $data->panp }}</a></td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td><strong>Aadhar card / driving licence / Voter Card</strong></td>
            <td><a href="{{ url('docs/'.$data->idp) }}" title="Click to view" target="_blank" data-saferedirecturl="{{ url('docs/'.$data->idp) }}">{{ $data->idp }}</a></td>
        </tr>
        <tr bgcolor="#EAF2FA">
            <td><strong>Bndwidth</strong></td>
            <td>{{ $data->bandw }} Mbps</td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td><strong>Service Location</strong></td>
            <td>{{ $data->loca }}</td>
        </tr>
        <tr bgcolor="#EAF2FA">
            <td><strong>Billing Address</strong></td>
            <td>{{ $data->staddr }},<br/>
            {{ $data->addr }},<br/>
            {{ $data->city }} - {{ $data->zipn }}<br/>
            {{ $data->state }}
            </td>
        </tr>
        @if($data->gstn != "")
        <tr bgcolor="#FFFFFF">
            <td><strong>GSTN</strong></td>
            <td>{{ $data->gstn }}</td>
        </tr>
        @endif
    </tbody>
</table>
</body>
</html>