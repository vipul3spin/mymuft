<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Suppor Ticket Status</title>
</head>
<body>
    <p>
        Hello {{ ucfirst($ticketOwner->name) }},
    </p>
    <p>
        Your support ticket with ID <b>[ #{{ $ticket->ticket_id }} ]</b> has been marked as resolved and closed.
    </p>
</body>
</html>