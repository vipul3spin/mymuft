<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Logout Session Information</title>
</head>
<body>
******************************************
<br/>
Scheduled Job Completed.
<br/>
DateTime : {{ $datetime }}
<br/>
Free Session Resets : {{ $sysreset }}
<br/>
******************************************
</body>
</html>