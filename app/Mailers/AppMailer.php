<?php

namespace App\Mailers;

use App\Models\Ticket;
use Illuminate\Contracts\Mail\Mailer;

class AppMailer {
    protected $mailer; 
    protected $fromAddress = 'no-reply@muftwifi.com';
    protected $fromName = 'Muft Internet';
    protected $to;
    protected $subject;
    protected $view;
    protected $data = [];
    protected $cc;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendTicketInformation($user, Ticket $ticket)
    {
        $this->to = $user->email;
        $this->subject = "[Ticket ID: #$ticket->ticket_id] $ticket->title";
        $this->view = 'emails.ticket_info';
        $this->data = compact('user', 'ticket');

        return $this->deliver();
    }
    
    public function sendTicketComments($ticketOwner, $user, Ticket $ticket, $comment)
    {
        $this->to = $ticketOwner->email;
        $this->subject = "RE: $ticket->title [Ticket ID: #$ticket->ticket_id]";
        $this->view = 'emails.ticket_comments';
        $this->data = compact('ticketOwner', 'user', 'ticket', 'comment');

        return $this->deliver();
    }
    
    public function sendTicketStatusNotification($ticketOwner, Ticket $ticket)
    {
        $this->to = $ticketOwner->email;
        $this->subject = "RE: $ticket->title [Ticket ID: #$ticket->ticket_id]";
        $this->view = 'emails.ticket_status';
        $this->data = compact('ticketOwner', 'ticket');

        return $this->deliver();
    }

    public function deliver()
    {
        $this->mailer->send($this->view, $this->data, function($message) {
            $message->from($this->fromAddress, $this->fromName)
                    ->to($this->to)->subject($this->subject);
        });
    }

    public function deliverwithcc()
    {
        $this->mailer->send($this->view, $this->data, function($message) {
            $message->from($this->fromAddress, $this->fromName)
                    ->to($this->to)->cc($this->cc)->subject($this->subject);
        });
    }    

    public function sendcustmail($text, $subj, $user, $cce)
    {
        $this->to = $user->email;
        $this->subject = $subj;
        $this->view = 'emails.send_html_mail';
        $this->cc = $cce;
        $this->data = compact('text');
        if($cce != ""){
                $this->deliverwithcc();
        } else {
            return $this->deliver();
        }

        //$this->data = $text;
        //return $this->deliverraw();
    }

    public function sendadminmail($text, $subj)
    {
        $this->to = 'vipul@3sp.in';//env('MAIL_FROM_EMAIL');
        $this->subject = $subj;
        $this->view = 'emails.send_html_mail';

        $this->data = compact('text');
        return $this->deliver();

        //$this->data = $text;
        //return $this->deliverraw();
    }

    public function deliverraw()
    {
        $this->mailer->raw($this->data,function($message) {
            $message->from($this->fromAddress, $this->fromName)
                    ->to($this->to)->subject($this->subject);
        });
    }

    public function orderadmin($data, $subj)
    {
        $this->to = 'vipul@3sp.in';//env('MAIL_FROM_EMAIL');
        $this->subject = $subj;
        $this->view = 'emails.send_order_admin';
        $this->data = compact('data');

        return $this->deliver();        
    }

    public function ordercust($data, $subj)
    {
        $this->to = $data->cemail;
        $this->subject = $subj;
        $this->view = 'emails.send_order_customer';
        $this->data = compact('data');

        return $this->deliver();        
    }    
}