<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;
use DB;
use Mail;
use Log;
use DateInterval;
use Dwij\Laraadmin\Models\LAConfigs;
use App\Models\Employee;
use App\Models\Customer;
use App\Models\Pricing_Plan;

class Subscription extends Model
{
    use SoftDeletes;
	
	protected $table = 'subscriptions';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    /**
     * Add Plan
     */
    public static function addplan($id,$plan)
    {   
        $ext_act = DB::table('subscriptions')->where('cust_name','=',$id)->whereNull('deleted_at')->orderBy('renew_date','desc')->limit(1)->get();
        if(!empty($ext_act) && count($ext_act) > 0){
            $dtdiff = Subscription::getdatediff(strtotime('now'),strtotime($ext_act[0]->renew_date));
            if($dtdiff == 0 || $dtdiff > 0){
                $ndate = date_create($ext_act[0]->renew_date);
                $ndate->add(new DateInterval('P1D'));
                $act_date = $ndate->format('Y-m-d H:i:s');
                $rev_date = $ndate;
            } else { 
                $act_date = new DateTime();
                $rev_date = new DateTime();
            } 
        } else {
            $act_date = new DateTime();
            $rev_date = new DateTime();
        }
        $days = 28;
        $rev_date->add(new DateInterval('P'.$days.'D'));
		$rev_date = $rev_date->format('Y-m-d H:i:s');
		$result = DB::table('subscriptions')->insertGetId(['created_at'=> new DateTime(), 'updated_at' => new DateTime(), 'cust_name' => $id, 'plan' => $plan, 'act_date' => $act_date, 'renew_date' => $rev_date]);
		
        $resultmail = Subscription::sendplanmail($id, $plan);
    }

    public static function sendplanmail($id, $plan){
        if($id != NULL && $plan != NULL) {
            $user = Customer::find($id);
            $plan = Pricing_Plan::find($plan);
            if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
                // Send mail to User his Credit Balance
                Mail::send('emails.send_add_plan', ['username' => $user->name, 'plan' => $plan], function ($m) use ($user) {
                    $m->from(env('MAIL_FROM_EMAIL'), env('MAIL_FROM_NAME'));
                    $m->to($user->email, $user->name)->subject(LAConfigs::getByKey('sitename').' Muft Internet - New Plan Purchased');
                    });
            } else {
                Log::info("Low Credit Balance: username: ".$user->email." Plan: ".$plan);
            }   
        }
    }
    
    public static function getdatediff($date1,$date2)
    {
        $date1 = strtotime(date("Y-m-d",$date1));
        $date2 = strtotime(date('Y-m-d',$date2));
        $datediff = floor(($date2 - $date1)/(60*60*24));
        return $datediff;
    }    
}
