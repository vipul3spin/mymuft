<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;
	
	protected $table = 'tickets';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public static function gettotal($id = NULL) {
        if($id == Null) {
            $tticket = Ticket::count('id');
        } else {
            $tticket = Ticket::where(['cust_id'=>$id])->whereNull('deleted_at')->count('id');
        }
        if($tticket > 0)
        {
            return $tticket;
        } else {
            return 0;
        }        
    }
    
    public static function getcount($status,$id = NULL) {
        if($id == Null) {
            $cticket = Ticket::where(['status'=>$status])->count('id');
        } else {
            $cticket = Ticket::where(['cust_id'=>$id,'status'=>$status])->count('id');
        }
        if($cticket > 0)
        {
            return $cticket;
        } else {
            return 0;
        }        
    } 	
}
