<?php

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;
use Razorpay\Api\Api;
use Session;
use Redirect;
use DB;
use DateInterval;
use Carbon\Carbon;
use Mail;
use Log;
use Dwij\Laraadmin\Models\LAConfigs;
use DateTime;

use Entrust;
use App\Models\Payment;
use App\Models\Customer;
use App\Models\Transaction;
use App\Models\Subscription;

class RazorpayController extends Controller
{    
    public function payment()
    {
        //Input items of form
        $input['razorpay_payment_id'] = Input::all();
        
        $razorpay_payment_id = Input::get('razorpay_payment_id');

      if($razorpay_payment_id == "null") {
            $desc = "Transaction Failed.[ OrderID: ".Input::get('mworder_id')." ]";
            return redirect(config('laraadmin.adminRoute')."/")->withErrors($desc);
      } else {
        if(count($input)  && !empty($razorpay_payment_id)) {
            $result = $this->updatepayment($razorpay_payment_id);
            if(isset($result['status'])) {
                $status = $result['status'];
                switch($status) {
                    case 'Success': $msgtype = 'success_msg';
                                    break;
                    case 'Pending': $msgtype = 'warning_msg';
                                    break;
                    case 'Failed': $msgtype = 'failed_msg';
                                    break;
                    default: $msgtype = 'failed_msg';
                             break;
                }
                return redirect(config('laraadmin.adminRoute')."/")->with($msgtype, $result['respmsg']);
            } else {
                return redirect(config('laraadmin.adminRoute')."/")->withErrors($result);
            }
        } else {
            return redirect(config('laraadmin.adminRoute')."/")->withErrors('Please Contact Muft WiFi');
        }
      }
    }
    
    public static function updatepayment($razorpayment_id)
    {
        $api = new Api(config('razorpay.razor_key'), config('razorpay.razor_secret'));
        try {
            $response = $api->payment->fetch($razorpayment_id);
            $responseorder = $api->order->fetch($response->order_id)->payments();
            $payid = Payment::where('pay_order_id','=',$response->order_id)->value('id');

            $payment = Payment::find($payid);

            if(isset($response->id))                { $payment->pay_id          = $response->id;                        }
            if(isset($responseorder->count))        { $payment->attempts        = $responseorder->count;                }            
            if(isset($response->currency))          { $payment->currency        = $response->currency;                  }
            if(isset($response->status))            { $payment->status          = $response->status;                    }
            if(isset($response->invoice_id))        { $payment->pay_invoice_id  = $response->invoice_id;                } else { $payment->pay_invoice_id = "N/A"; }
            if(isset($response->international))     { $payment->international   = ($response->international) ? "Yes" : "No"; }
            if(isset($response->method))            { $payment->method          = $response->method;                    }
            if(isset($response->description))       { $payment->description     = $response->description;               }
            if(isset($response->card_id))           { $payment->card_id         = $response->card_id;                   } else { $payment->card_id = "N/A"; }
            if(isset($response->bank))              { $payment->bank            = $response->bank;                      } else { $payment->bank = "N/A"; }
            if(isset($response->wallet))            { $payment->wallet          = $response->wallet;                    } else { $payment->wallet = "N/A"; }
            if(isset($response->vpa))               { $payment->vpa             = $response->vpa;                       } else { $payment->vpa = "N/A"; }
            if(isset($response->email))             { $payment->email           = $response->email;                     }
            if(isset($response->contact))           { $payment->contact         = $response->contact;                   }
            if(isset($response->fee))               { $payment->fee             = $response->fee/100;                   }
            if(isset($response->tax))               { $payment->tax             = $response->tax/100;                   }
            if(isset($response->error_code))        { $payment->error_code      = $response->error_code;                } else { $payment->error_code = "N/A"; }
            if(isset($response->error_description)) { $payment->error_description       = $response->error_description; } else { $payment->error_description = "N/A"; }
            if(isset($response->created_at))        { $payment->txn_date        = date("Y-m-d H:i:s", $response->created_at); }

            $payment->save();
            
            $data = RazorpayController::updatecredits($payment);
            
            return $data;
        } catch (\Exception $e) {    
            //return  $e->getMessage();
            // \Session::put('error',$e->getMessage());
            $msg = $e->getMessage();
            return $msg;
        }
    } 
    
    public static function updatecredits($payment)
    {
        $type = "Add Credit";
        $userid = $payment->user_id;

        if($payment->status == "captured"){
            //Transaction Successful
            $desc = "Plan purchased sucessfully.[ OrderID: $payment->order_id".( !empty($payment->pay_id) ? ", TxnID: $payment->pay_id" : "" )." ]";                
            $result = Subscription::addplan($userid, $payment->plan);
            $status = "Success";
        }else if($payment->status == "failed"){
            //Transaction Failed
            $desc = "Transaction Failed.[ OrderID: $payment->order_id".( !empty($payment->pay_id) ? ", TxnID: $payment->pay_id" : "" )." ]"; 
            //$result = Transaction::updatecredit($userid, 0, $type, $desc);
            $status = "Failed";
        }else if($payment->status == "created" || $payment->status == "authorized"){
            //Transaction Open/Processing
            $desc = "Transaction Under Process.[ OrderID: $payment->order_id ]"; 
            //$result = Transaction::updatecredit($userid, 0, $type, $desc);
            $status = "Pending";
        } 

        $usr_id = Customer::find($userid);
        $c_id = "c=".$usr_id->link_id;
        $user = DB::table('customers')->where('id','=',$c_id)->whereNull('deleted_at')->get();

        if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
            // Send mail to User his new Password
            Mail::send('emails.send_credit_purchase_payment',['username' => $user->name,'orderid' => $payment->order_id,'datetime' => $payment->txn_date,'credit_purchase' => $payment->buy_credits,'txnamt' => $payment->txnamt,'txnid' => $payment->pay_id,'status' => $status,'msg' => $payment->error_description], function ($m) use ($user) {
            $m->from(env('MAIL_FROM_EMAIL'), env('MAIL_FROM_NAME'));
            $m->to($user->email, $user->name)->subject(LAConfigs::getByKey('sitename').' Hotspot Solution - Order Details/Status');
            });
        } else {
            Log::info("Order Details-Status: OrderID: ".$user->email." Date: ".$payment->txn_date);
        }            

        $data['status'] = $status;
        $data['txnid'] = ( !empty($payment->pay_id) ? $payment->pay_id : "" );
        $data['respmsg'] = $desc;

        return $data;

        //$desc = "Credit purchased sucessfully.";
    }
        
    public function vieworder() {
        return view('test');
    }
    
    public function plans() {
        $api = new Api(config('razorpay.razor_key'), config('razorpay.razor_secret'));
        try {
            $response = $api->plan->fetch('plan_B9sn8n0hNWHCdx');
            //$responseorder = $api->order->fetch($response->order_id)->payments();
            //$payid = Payment::where('pay_order_id','=',$response->order_id)->value('id');

            //$payment = Payment::find($payid);

            
            $data = (array)$response->item;
            
            return $data;
        } catch (\Exception $e) {    
            //return  $e->getMessage();
            // \Session::put('error',$e->getMessage());
            $msg = $e->getMessage();
            return $msg;
        }        
    }
    
    public function userplan(){
//        $api = new Api(config('razorpay.razor_key'), config('razorpay.razor_secret'));
//        try {
////            $data["plan_id"] = "plan_B9sn8n0hNWHCdx";
////            $data["customer_notify"] = 1;
////            $data["total_count"] = 6;            
////            
////            $response = $api->Subscription->create($data);
//            //$responseorder = $api->order->fetch($response->order_id)->payments();
//            //$payid = Payment::where('pay_order_id','=',$response->order_id)->value('id');
//
//            //$payment = Payment::find($payid);
//            $response = $api->Subscription->fetch('sub_B9tQXYOAqopGAP');
//            
//            $data = (array)$response;
//            
//            return $data;
//        } catch (\Exception $e) {    
//            //return  $e->getMessage();
//            // \Session::put('error',$e->getMessage());
//            $msg = $e->getMessage();
//            return $msg;
//        }                  
                                
                                $data = [
                                    "key"               => config('razorpay.razor_key'),
                                    "subscription_id"            => 'sub_B9tQXYOAqopGAP',
                                    "name"              => LAConfigs::getByKey('sitename_part1')." ".LAConfigs::getByKey('sitename_part2'),
                                    "description"       => "User Plan Scbscribe",
                                    "image"             => asset('la-assets/img/muftwifi.png'),
                                    "prefill"           => [
                                        "name"              => 'Vipul',
                                        "email"             => 'vipul@3sp.in',
                                    ],
                                ];
                                                              
                                //return response()->json($data);
                                
                                return view('la.payWithRazorpaysub')->with('data',json_encode($data));        
    }
}