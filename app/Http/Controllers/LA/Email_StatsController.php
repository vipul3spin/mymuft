<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Email_Category;
use App\Models\Contact;

class Email_StatsController extends Controller
{
	public $show_action = false;
	public $view_col = 'Name';
	public $listing_cols = ['Id','Send Date', 'Name', 'Subject'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				//$this->listing_cols = ModuleFields::listingColumnAccessScan('Email_Stats', $this->listing_cols);
				return $next($request);
			});
		} else {
			//$this->listing_cols = ModuleFields::listingColumnAccessScan('Email_Stats', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Email_Stats.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//$module = Module::get('Email_Stats');
		
		///if(Module::hasAccess($module->id)) {
			return View('la.email_stats.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols
				//'module' => $module
			]);
		//} else {
       //     return redirect(config('laraadmin.adminRoute')."/");
       // }
	}

	/**
	 * Show the form for creating a new email_category.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		//$values = DB::table('Email_Stats')->select($this->listing_cols)->whereNull('deleted_at');
		$values = DB::table('email_sent_stats')->select('id','created_at','cont_id','email_subj')->orderBy('created_at', 'DESC');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		//$fields_popup = ModuleFields::getModuleFields('Email_Stats');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($col == $this->view_col) {
					//$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/email_stats/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				if($col == "Name") {
					$cont_name = $this->getcontname($data->data[$i][$j]);
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/contacts/'.$data->data[$i][$j]).'">'.$cont_name.'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
				if($col == 'Date') {
					$data->data[$i][$j] = Carbon::createFromFormat('Y-m-d H:i:s', $data->data[$i][$j])->format('d-m-Y');
				}	

			}
		}
		$out->setData($data);
		return $out;
	}

	public function getcontname($cont_id)
	{
		$contact = Contact::find($cont_id);

		return $contact->name;
	}
}
