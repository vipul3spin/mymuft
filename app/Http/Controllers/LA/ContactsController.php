<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Carbon\Carbon;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Contact;
use App\Models\Contact_Timeline;
use App\Models\Email_Template;

class ContactsController extends Controller
{
	public $show_action = false;
	public $view_col = 'Name';
	//public $listing_cols = ['id', 'choice', 'name', 'email', 'mmobno', 'vnamed', 'bandwidth', 'icapacity', 'vnoc', 'rcomp', 'ispl', 'cname'];
	//public $listing_cols = ['id', 'choice', 'name', 'email', 'mmobno'];
	public $listing_cols = ['ID','Date','Name','Service','Location','Mobile'];

	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				//$this->listing_cols = ModuleFields::listingColumnAccessScan('Contacts', $this->listing_cols);
				return $next($request);
			});
		} else {
			//$this->listing_cols = ModuleFields::listingColumnAccessScan('Contacts', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Contacts.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Contacts');
		
		if(Module::hasAccess($module->id)) {
			return View('la.contacts.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new contact.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created contact in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Contacts", "create")) {
		
			$rules = Module::validateRules("Contacts", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Contacts", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.contacts.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified contact.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Contacts", "view")) {
			
			$contact = Contact::find($id);
			if(isset($contact->id)) {
				$module1 = Module::get('Contact_Timelines');

				$module = Module::get('Contacts');
				$module->row = $contact;
				$get_contids = DB::table('contacts')->where('mmobno','=',$contact->mmobno)->whereNull('deleted_at')->pluck('id');

				$tim_data = DB::table('contact_timelines')->whereIn('cont_id',$get_contids,'and')->orderBy('id', 'DESC')->get();
				
				return view('la.contacts.show', [
					'module' => $module,
					'module1' => $module1,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding",
				])->with(['contact'=> $contact,'ctimeline' => $tim_data]);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("contact"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified contact.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Contacts", "edit")) {			
			$contact = Contact::find($id);
			if(isset($contact->id)) {	
				$module = Module::get('Contacts');
				
				$module->row = $contact;
				
				return view('la.contacts.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('contact', $contact);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("contact"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified contact in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Contacts", "edit")) {
			
			$rules = Module::validateRules("Contacts", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Contacts", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.contacts.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified contact from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Contacts", "delete")) {
			Contact::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.contacts.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		//$values = DB::table('contacts')->select($this->listing_cols)->whereNull('deleted_at')->orderBy('id', 'DESC');
		$values = DB::table('contacts')->select('id','created_at','name','choice', 'vnamed', 'mmobno')->whereNull('deleted_at');//->orderBy('created_at', 'DESC');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/contacts/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}

				if($col == 'Date') {
					$data->data[$i][$j] = Carbon::createFromFormat('Y-m-d H:i:s', $data->data[$i][$j])->format('d-m-Y');
				}				
				if($col == 'Mobile') {
					//$data->data[$i][$j] = '<a href="tel:'.$data->data[$i][$j].'">'.$data->data[$i][$j].'</a>';
				}
			}
		}


		//$fields_popup = ModuleFields::getModuleFields('Contacts');
		
		// for($i=0; $i < count($data->data); $i++) {
		// 	for ($j=0; $j < count($this->listing_cols); $j++) { 
		// 		$col = $this->listing_cols[$j];
		// 		if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
		// 			$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
		// 		}
		// 		if($col == $this->view_col) {
		// 			$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/contacts/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
		// 		}
		// 		// else if($col == "author") {
		// 		//    $data->data[$i][$j];
		// 		// }
		// 	}
			
		// 	if($this->show_action) {
		// 		$output = '';
		// 		$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/contacts/'.$data->data[$i][0]).'"><button class="btn btn-success btn-xs" type="submit"><i class="fa fa-eye"></i></button></a>';
		// 		if(Module::hasAccess("Contacts", "edit")) {
		// 			//$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/contacts/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
		// 			//$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/contacts/'.$data->data[$i][0].'/edit').'" style="margin-left:3px;"><button class="btn btn-warning btn-xs" type="submit"><i class="fa fa-edit"></i></button></a>';
		// 		}
				
		// 		if(Module::hasAccess("Contacts", "delete")) {
		// 			//$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.contacts.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
		// 			//$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
		// 			//$output .= Form::close();
		// 		}
		// 		$data->data[$i][] = (string)$output;
		// 	}
		// }
		$out->setData($data);
		return $out;
	}
}
