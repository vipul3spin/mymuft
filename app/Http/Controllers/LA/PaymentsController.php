<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Carbon\Carbon;
use Validator;
use Datatables;
use DateTime;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Mail;
use Log;
use Dwij\Laraadmin\Models\LAConfigs;

use Entrust;
use App\Models\Payment;
use Razorpay\Api\Api;
use App\Models\Transaction;
use App\Models\Customer;
use App\Http\Controllers\LA\RazorpayController;

class PaymentsController extends Controller
{
	public $show_action = false;
	public $view_col = 'order_id';
        public $listing_cols = ['id', 'txn_date', 'order_id', 'user_id', 'plan', 'txnamt','attempts','status'];
	//public $listing_cols = ['id', 'order_id', 'user_id', 'plan', 'amt', 'sertax_par', 'sramt', 'tax_par', 'taxamt', 'txnamt', 'txn_date', 'm_id', 'pay_id', 'currency', 'status', 'pay_order_id', 'pay_invoice_id', 'international', 'method', 'description', 'card_id', 'bank', 'wallet', 'vpa', 'email', 'contact', 'fee', 'tax', 'error_code', 'error_description'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Payments', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Payments', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Payments.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Payments');
		
		if(Module::hasAccess($module->id)) {
			return View('la.payments.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new payment.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created payment in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Payments", "create")) {
		
			$rules = Module::validateRules("Payments", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Payments", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.payments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified payment.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Payments", "view")) {
			
			$payment = Payment::find($id);
			if(isset($payment->id)) {
				
				if($payment->attempts == 0 || $payment->status == "" || $payment->status == null || $payment->status == "pending"){
					$chkpaystatus = $this->statusCheck($payment->pay_order_id);
				}
				$payment = Payment::find($id);
				$emp = Customer::where('id','=',$payment->user_id)->whereNull('deleted_at')->value('name');
				$payment['user_name'] = $emp;
                                
				$module = Module::get('Payments');
				$module->row = $payment;                            
                            				
				return view('la.payments.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding",
                                        'payments' => $payment
				])->with('payment', $payment);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("payment"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified payment.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Payments", "edit")) {			
			$payment = Payment::find($id);
			if(isset($payment->id)) {	
				$module = Module::get('Payments');
				
				$module->row = $payment;
				
				return view('la.payments.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('payment', $payment);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("payment"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified payment in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Payments", "edit")) {
			
			$rules = Module::validateRules("Payments", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Payments", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.payments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified payment from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Payments", "delete")) {
			Payment::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.payments.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('payments')->select($this->listing_cols)->whereNull('deleted_at')->orderBy('id','desc');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Payments');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/payments/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
                                
//                                if($col == "status") {
//                                    if($data->data[$i][$j] == "captured"){
//                                        $data->data[$i][$j] = "Success";
//                                    } elseif(!empty($data->data[$i][$j])) {
//                                        $data->data[$i][$j] = "Pending";
//                                    } else {
//                                        $data->data[$i][$j] = "Failed";
//                                    }
//                                }

                                if($col == 'status') {                                    
                                    if(empty($data->data[$i][$j])){
                                        $data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/payments/'.$data->data[$i][0]).'">View</a>';
                                        //$data->data[$i][6] = $txnstatus->status;
                                    } elseif($data->data[$i][$j] == "captured") {
                                        $data->data[$i][$j] = "<span class='status-label label label-success'>Success</span>";
                                    } elseif($data->data[$i][$j] == "failed") {
                                        $data->data[$i][$j] = "<span class='status-label label label-danger'>Failed</span>";
                                    } else {
                                        $data->data[$i][$j] = "<span class='status-label label label-warning'>Pending</span>";
                                    }
                                }
                               
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Payments", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/payments/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Payments", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.payments.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
        
        public function statusCheck($pay_order_id) {
            $api = new Api(config('razorpay.razor_key'), config('razorpay.razor_secret'));
            
            try {
                $response = $api->order->fetch($pay_order_id)->payments();                 
            } catch (\Exception $e) {
                //return  $e->getMessage();
               // \Session::put('error',$e->getMessage());
                $msg = $e->getMessage();
                return $msg;
            }
            
            $count = $response->count;
            $items = $response->items;
            $created_at = 0;            

            if($count > 0) {
                for ($i = 0; $i < $count; $i++){
                    if($items[$i]->created_at > $created_at){
                        $created_at = $items[$i]->created_at;
                        $payment_id = $items[$i]->id;
                    }
                }
                $result = RazorpayController::updatepayment($payment_id);
            } else {
                try {
                    $response = $api->order->fetch($pay_order_id);                 
                } catch (\Exception $e) {
                    //return  $e->getMessage();
                   // \Session::put('error',$e->getMessage());
                    $msg = $e->getMessage();
                    return $msg;
                }                
                
		if($response->status == "paid") {
		    $result = $this->statusCheck($pay_order_id);
		} else {
                    $now = Carbon::now()->subMinutes(30);
                    $creatediff = $now->gt(Carbon::parse(date("Y-m-d H:i:s",$response->created_at)));

                    $payid = Payment::where('pay_order_id','=',$pay_order_id)->value('id');

                    $payment = Payment::find($payid);
                    $payment->status = $stat = (($creatediff) ? "failed" : "pending" );
                    $payment->save();

                    return $stat;
		}                    
            } 
        }
}
