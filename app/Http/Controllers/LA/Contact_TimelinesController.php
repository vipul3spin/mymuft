<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Mailers\AppMailer;

use App\Models\Contact_Timeline;
use App\Models\Contact;

class Contact_TimelinesController extends Controller
{
	public $show_action = true;
	public $view_col = 'cont_id';
	public $listing_cols = ['id', 'cont_id', 'type', 'title'];//, 'description'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Contact_Timelines', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Contact_Timelines', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Contact_Timelines.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Contact_Timelines');
		
		if(Module::hasAccess($module->id)) {
			return View('la.contact_timelines.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new contact_timeline.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created contact_timeline in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request,AppMailer $mailer)
	{
		if(Module::hasAccess("Contact_Timelines", "create")) {
		
			$rules = Module::validateRules("Contact_Timelines", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			if($request->type == "Email"){
				$contact_info = Contact::find($request->cont_id);
				$subj = $request->title;
				$mail_body = $request->description;
				$cce_mails = $request->cc_emails;

				if($cce_mails != ""){
					$lines = explode(',', $cce_mails);
					$cc_array = [];
					foreach($lines as $key => $ut){
						if(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $ut)){
							$cc_array[] = $ut;
						}
					}
			    } else {
					$cc_array = "";
				}
				$mailer->sendcustmail($mail_body, $subj, $contact_info, $cc_array);

				$stas_data = [
					'cont_id' => $request->cont_id,
					'email_subj' => $subj,
					'created_at' => date("Y-m-d H:i:s")
				];
		
				$emailstas_ins = DB::table('email_sent_stats')->insertGetId($stas_data);

			}

			$insert_id = Module::insert("Contact_Timelines", $request);
			

			
			//return redirect()->route(config('laraadmin.adminRoute') . '.contact_timelines.index');
			//return redirect()->route(config('laraadmin.adminRoute') ."/"."contacts/".$request->cont_id);
			//return redirect(config('laraadmin.adminRoute')."/" ."contacts/".$request->cont_id)->with('success','Action added successfully'); 
			return back()->with('success','Action added successfully');
		} else {
			return redirect(config('laraadmin.adminRoute')."/")->with('error','Not Permitted');
		}
	}

	/**
	 * Display the specified contact_timeline.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Contact_Timelines", "view")) {
			
			$contact_timeline = Contact_Timeline::find($id);
			if(isset($contact_timeline->id)) {
				$module = Module::get('Contact_Timelines');
				$module->row = $contact_timeline;
				
				return view('la.contact_timelines.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('contact_timeline', $contact_timeline);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("contact_timeline"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified contact_timeline.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Contact_Timelines", "edit")) {			
			$contact_timeline = Contact_Timeline::find($id);
			if(isset($contact_timeline->id)) {	
				$module = Module::get('Contact_Timelines');
				
				$module->row = $contact_timeline;
				
				return view('la.contact_timelines.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('contact_timeline', $contact_timeline);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("contact_timeline"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified contact_timeline in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Contact_Timelines", "edit")) {
			
			$rules = Module::validateRules("Contact_Timelines", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Contact_Timelines", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.contact_timelines.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified contact_timeline from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Contact_Timelines", "delete")) {
			Contact_Timeline::find($id)->delete();
			$res['statusCode'] = 200;

			$data = json_encode($res);
			return $data;
			// Redirecting to index() method
			//return redirect()->route(config('laraadmin.adminRoute') . '.contact_timelines.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('contact_timelines')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Contact_Timelines');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/contact_timelines/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Contact_Timelines", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/contact_timelines/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Contact_Timelines", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.contact_timelines.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

	public function addcomment(Request $request,AppMailer $mailer)
	{
		if(Module::hasAccess("Contact_Timelines", "create")) {
		
			$rules = Module::validateRules("Contact_Timelines", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			if($request->type == "Email"){
				$contact_info = Contact::find($request->cont_id);
				$subj = $request->title;
				$mail_body = $request->description;
				$mailer->sendcustmail($mail_body, $subj, $contact_info);
			}

			$insert_id = Module::insert("Contact_Timelines", $request);
			

			$res['statusCode'] = 200;
			//return redirect()->route(config('laraadmin.adminRoute') . '.contact_timelines.index');
			//return redirect()->route(config('laraadmin.adminRoute') ."/"."contacts/".$request->cont_id);
			//return redirect(config('laraadmin.adminRoute')."/" ."contacts/".$request->cont_id);
			
		} else {
			$res['statusCode'] = 400;
			//return redirect(config('laraadmin.adminRoute')."/");
		}
		
		$data = json_encode($res);
		return $data;
	}	
}
