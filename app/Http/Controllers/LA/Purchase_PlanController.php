<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Auth;
use DB;
use Carbon\Carbon;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Dwij\Laraadmin\Models\LAConfigs;
use Razorpay\Api\Api;

use Entrust;
use App\User;
use App\Models\Payment;
use App\Models\Customer;
use App\Models\Pricing_Plan;
use App\Http\Controllers\LA\PaymentsController;

class Purchase_PlanController extends Controller
{
    public function purplan(Request $request) {
        if(!empty($request)) {
            $validuser = FALSE;
            $validplan = FALSE;
        
            if(isset($request->user_id)){
                $cust = Customer::find($request->user_id);
                $uname = preg_replace('/\W+/', '', $cust->name);
                $validuser = TRUE;
            } else {
                $usr = User::find(Auth::user()->id);
                $cust = Customer::where('id','=',str_replace("c-","",$usr->link_id));
                $uname = preg_replace('/\W+/', '', $cust->name);
                $validuser = TRUE;
            }
            
            if(empty($request->plan) && ($request->plan < 0) && ($request->plan == 0)){
                $validplan = FALSE;
            } else {
                if(!empty($request->plan) && ($request->plan > 0)) {
                    $validplan = TRUE;
                } else {
                    $validplan = FALSE;
                }
            }
            
            if($validuser) {
                if($validplan) {
                        $chkorder = 1;

                        while($chkorder > 0){
                            $order_id = "MWORDS".mt_rand(00000, 999999999);
                            if($this->chkorderid($order_id)){
                                $chkorder = 0;
                            } else {
                                $chkorder = 1;
                            }
                        }

                        $plan_data = Pricing_Plan::find($request->plan);
                        $amt = $plan_data->plan_fig * $plan_data->price;
                        $sramt = 0; //(LAConfigs::getByKey('service_chrg'))/100 * $amt;
                        $taxamt = $amt * (LAConfigs::getByKey('gst'))/100;
                        $txnamount = $amt+$sramt+$taxamt;
                        $txnamt = number_format((float)$txnamount, 2, '.', '');
                        
                        $payment = Payment::create([
                                'order_id' => $order_id,
                                'user_id' => $cust->id,
                                'plan' => $plan_data->id,
                                'amt' => $amt,
                                'sertax_par' => 0,//LAConfigs::getByKey('service_chrg'),
                                'sramt' => $sramt,
                                'tax_par' => LAConfigs::getByKey('gst'),
                                'taxamt' => $taxamt,
                                'txnamt' => $txnamt,
                                'm_id' => config('razorpay.razor_key'),
                                'txn_date' => Carbon::now()
                        ]); 
                            
                            $api = new Api(config('razorpay.razor_key'), config('razorpay.razor_secret'));
                            
                            $orderData = [
                                'receipt'         => $payment->id,
                                'amount'          => $txnamt * 100, // 2000 rupees in paise
                                'currency'        => 'INR',
                                'payment_capture' => 1 // auto capture
                            ];
                            
                            $razorpayOrder = $api->order->create($orderData);
                            
                            $razorpayOrderId = $razorpayOrder['id'];
                            $displayAmount = $amount = $orderData['amount'];
                            
                            $data = [
                                "key_id"               => config('razorpay.razor_key'),
                                "amount"            => $amount,
                                "name"              => LAConfigs::getByKey('sitename_part1')." ".LAConfigs::getByKey('sitename_part2'),
                                "description"       => "Plan Purchase",
                                "image"             => asset('la-assets/img/muftwifi.png'),
                                "prefill"           => [
                                    "name"              => $uname,
                                    "email"             => $cust->email,
                                    "contact"           => $cust->mobile,
                                ],
                                "notes"             => [
                                    "mw_order_id"       => $order_id,
                                ],
                                "theme"             => [
                                    "color"             => "#F37254"
                                ],
                                "order_id"          => $razorpayOrderId,
                            ];
                            
                            $payment = Payment::find($payment->id);
                            $payment->pay_order_id = $razorpayOrderId;
                            $payment->save();
                                                            
                            //return response()->json($data);
                            
                            return view('la.payWithRazorpay',compact('payment'))->with('data',json_encode($data));
                } else {
                    return redirect()->back()->with('error','Please Check Credits or Amount properly'); //->withErrors("Please Check Credits or Amount properly");
                }
            } else {
                return redirect()->back()->withErrors("Please check user profile properly");
            }
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    public function chkorderid($order_id) {
        if($order_id == NULL){
            return false;
        } else {
            $orderid = DB::table('payments')->where('order_id','=',$order_id)->whereNull('deleted_at')->value('order_id');
            if(empty($orderid)){
                return true;
            } else {
                return false;
            }
        }
    }    
}