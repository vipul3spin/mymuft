<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Carbon\Carbon;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Mailers\AppMailer;

use Entrust;
use App\Models\Ticket;
Use App\Models\Comment;
use App\Models\Employee;
use App\Models\Customer;
use App\User;

class TicketsController extends Controller
{
	public $show_action = true;
	public $view_col = 'ticket_id';
	public $listing_cols = ['cust_id', 'title', 'cat_id', 'ticket_id', 'priority', 'status', 'created_at'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Tickets', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Tickets', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Tickets.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Tickets');
		
		if(Module::hasAccess("Tickets", "create")) {
			return View('la.tickets.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
        }
	}

	/**
	 * Show the form for creating a new ticket.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
            //
	}

	/**
	 * Store a newly created ticket in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request,AppMailer $mailer)
	{
		if(Module::hasAccess("Tickets", "create")) {
		
			$rules = Module::validateRules("Tickets", $request);
                        if(isset($request->cust_id)){
                            $request['cust_id'] = $custid = $request->cust_id;
                        } else {
                            $request['cust_id'] = $custid = Auth::user()->id;
                        }
                        
                        $request['ticket_id'] = $ticket_id = $this->genticketid();
                        
                        $request['status'] = "Open";
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Tickets", $request);
                        
						$ticket = Ticket::find($insert_id);
						
						$owner = Customer::find($custid);

                        $mailer->sendTicketInformation($owner, $ticket);
                        
			return redirect(config('laraadmin.adminRoute').'/tickets/'.$ticket_id)->with('success','Support ticket created successfully.');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
		}
	}

	/**
	 * Display the specified ticket.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($ticket_id)
	{
		if(Module::hasAccess("Tickets", "view")) {
                    $id = Ticket::where('ticket_id','=',$ticket_id)->value('id');
                    if($this->checkpermission($id)) {
			$ticket = Ticket::find($id);
			if(isset($ticket->id)) {
				$module = Module::get('Tickets');
				$module->row = $ticket;
				
                                //$comments = $ticket->comments;
                                $comments = DB::table('comments')->where('ticket_id',$ticket->id)->whereNull('deleted_at')->get();
                                //$comm = DB::table('comments')->where('ticket_id',$ticket->id)->value('id');
                                //$moc = Comment::find($comm);
                                $module1 = Module::get('Comments');
                                //$module1->row = $moc;
  
				return view('la.tickets.show', [
					'module' => $module,
					'view_col' => $this->view_col,
                                        'module1' => $module1
				])->with(compact('ticket','comments'));
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("ticket"),
				]);
			}
                    } else {
                            return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
                    }                        
		} else {
			return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
		}
	}

	/**
	 * Show the form for editing the specified ticket.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($ticket_id)
	{
		if(Module::hasAccess("Tickets", "edit")) {
                    $id = Ticket::where('ticket_id','=',$ticket_id)->value('id');
                    if($this->checkpermission($id)) {
			$ticket = Ticket::find($id);
			if(isset($ticket->id)) {	
				$module = Module::get('Tickets');
				
				$module->row = $ticket;
				
				return view('la.tickets.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('ticket', $ticket);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("ticket"),
				]);
			}
                    } else {
                            return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
                    }                        
		} else {
			return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
		}
	}

	/**
	 * Update the specified ticket in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $ticket_id)
	{
		if(Module::hasAccess("Tickets", "edit")) {
                    $id = Ticket::where('ticket_id','=',$ticket_id)->value('id');
                    if($this->checkpermission($id)) {
			$rules = Module::validateRules("Tickets", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Tickets", $request, $id);
			
			//return redirect()->route(config('laraadmin.adminRoute') . '.tickets.index');
                        
                        return redirect(config('laraadmin.adminRoute').'/tickets/'.$ticket_id)->with('success','Support ticket updated successfully.');                        
                    } else {
                            return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
                    }                        
		} else {
			return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
		}
	}

	/**
	 * Remove the specified ticket from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($ticket_id)
	{
		if(Module::hasAccess("Tickets", "delete")) {
                    $id = Ticket::where('ticket_id','=',$ticket_id)->value('id');
                    if($this->checkpermission($id)) {
			Ticket::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.tickets.index');
                    } else {
                            return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
                    }                        
		} else {
			return redirect(config('laraadmin.adminRoute')."/")->with('error','Permission denied');
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
            if(Entrust::hasRole('SUPER_ADMIN')){
		$values = DB::table('tickets')->select($this->listing_cols)->whereNull('deleted_at');
            } else {
                $values = DB::table('tickets')->select($this->listing_cols)->where('cust_id',"=",Auth::user()->id)->whereNull('deleted_at');
            }
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Tickets');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
                                    $urlc = $data->data[$i][$j];
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/tickets/'.$data->data[$i][$j]).'">#'.$data->data[$i][$j].'</a>';
				}

				if($col == 'title') {
					$data->data[$i][$j] = substr($data->data[$i][$j],0,25)."...";
				}
                                
                                if($col == 'status') {
                                    if($data->data[$i][$j] == "Open"){
                                        $data->data[$i][$j] = "<span class='label label-success'>".$data->data[$i][$j]."</span>";
                                    } elseif($data->data[$i][$j] == "On Hold") {
                                        $data->data[$i][$j] = "<span class='label label-warning'>".$data->data[$i][$j]."</span>";
                                    } else {
                                        $data->data[$i][$j] = "<span class='label label-danger'>".$data->data[$i][$j]."</span>";
                                    }
                                }
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Tickets", "view")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/tickets/'.$urlc).'" class="btn btn-success btn-xs" title="View" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-eye"></i></a>';
				}

				if(Module::hasAccess("Tickets", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/tickets/'.$urlc.'/edit').'" class="btn btn-warning btn-xs" title="Edit" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Tickets", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.tickets.destroy', $urlc], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" title="Delete" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
        
    public function close(Request $request,AppMailer $mailer)
	{
            $ticket_id = $request->ticket_id;
            
            $id = Ticket::where('ticket_id','=',$ticket_id)->value('id');

            $ticket = Ticket::find($id);
            
            $ticket->status = 'Closed';

            $ticket->save();

            $userid = $ticket->cust_id;
            
            $ticketOwner = User::find($userid);

            $mailer->sendTicketStatusNotification($ticketOwner, $ticket);

            return redirect()->back()->with("success", "Support ticket closed");
	}
        
    public function genticketid(){
            
            $tickt_id = strtoupper(str_random(10));
            
            $result = Ticket::where('ticket_id','=',$tickt_id)->value('ticket_id');
            
            if(count($result) < 1){
                return $tickt_id;
            } else {
                $tickid = $this->genticketid();
            }
        }
        
    public function checkpermission($id) {
        $result = Ticket::where(['id'=>$id, 'cust_id'=>Auth::user()->id])->get();
        if(count($result) > 0){
            if(($result[0]->cust_id == Auth::user()->id) || Entrust::hasRole('SUPER_ADMIN')) {
                   return true;
            } else {
                   return false;
            }
        } elseif(Entrust::hasRole('SUPER_ADMIN')) {
            return true;
        } else {
	    	return false;
		}
    }        
}
