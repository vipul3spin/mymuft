<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use Auth;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models;
use Cookie;
use DateTime;
use Entrust;
Use User;

use App\Models\Contact;
use App\Models\Contact_Timeline;
use App\Models\Email_Template;
use App\Models\Email_Category;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $date = Carbon::now()->format('Y-m-d');
        $data['total_lead'] = Contact::count();
        $data['today_lead'] = Contact::where("created_at",'like',$date. '%')->count();
        $data['total_email_templ'] = Email_Template::count();
        $data['total_email_cat'] = Email_Category::count();
        $data['total_email_sent'] = DB::table('email_sent_stats')->Count();
        $data['email_sent_today'] = DB::table('email_sent_stats')->where("created_at",'like',$date. '%')->Count();
        // if(Entrust::hasRole('SUPER_ADMIN')) {
        //     $data['activeplan'] = $this->getactiveplan();
        //     $data['totallogins'] = $this->getlogins("total");
        //     $data['todaylogins'] = $this->getlogins("today");

        // } else {
        //     $data['activerouter'] =$this->getactiverouter(Auth::user()->id);
        //     $data['totallogins'] = $this->getlogins("total",Auth::user()->id);
        //     $data['todaylogins'] = $this->getlogins("today",Auth::user()->id);

        // }

        $mchartdata = $this->getmapdata(30);
        $wchartdata = $this->getmapdata(7);
        $tchartdata = $this->getmapdata(0);

        return view('la.dashboard',compact(array('data','mchartdata','wchartdata','tchartdata')));        
        //return view('la.dashboard');
    }

    public function getactiveplan($id = NULL)
    {
        // if($id == NULL) {
        //     $values = DB::table('routers')->whereNull('deleted_at')->lists('id');
        // } else {
        //     $values  = SessionsController::getrouterslist($id);
        // }
        // return $values;
    }

    public function getcustid($id = NULL){
        // if($id == NULL) {
        //     $values = Auth::user()->id;
        // } else {
        //     $usr = User::find($id);
        //     if(!empty($usr)){
        //         if($usr->type == "Employee"){
                    
        //         }
        //     } else {
        //         $values = Auth::user()->id;
        //     }
        // }
        // return $values;        

    }

    public function getmapdata($days,$id = NULL)
    {
        if($days == 0){
            $date = Carbon::now()->format('Y-m-d 00:00');
            $datea = $this->timeRange( $date, Carbon::now()->format('Y-m-d H:i') );
        } else {
            $date = Carbon::now()->subDays($days)->format('Y-m-d');
            $datea = $this->dateRange( $date, Carbon::now()->format('Y-m-d') );
        } 
        if($days == 0){
            $values1 = Contact::select(\DB::raw('HOUR(created_at) AS cdate, COUNT(created_at) AS count'))
                    ->whereDate('created_at','=',$date)
                    ->groupBy('cdate')
                    ->get();
            
            foreach($datea as $key => $value) {
                $datea[$key]['cdata'] = 0;
                foreach($values1 as $val1){
                    if($value['date'] == $val1->cdate){
                        $datea[$key]['cdata'] = $val1->count;
                    }            
                }
                $datea[$key]['date'] = $value['date']."-".str_pad(($value['date']+1), 2, '0', STR_PAD_LEFT);
            }          
             
        } else {            
            $values1 = Contact::select(\DB::raw('date(created_at) AS cdate, COUNT(created_at) AS count'))
                    ->where('created_at','>',$date)
                    ->groupBy('cdate')
                    ->get();

            foreach($datea as $key => $value) {
                $datea[$key]['cdata'] = 0;
                foreach($values1 as $val1){
                    if(strtotime($value['date']) == strtotime($val1->cdate)){
                        $datea[$key]['cdata'] = $val1->count;
                    }            
                }
            } 
            
            foreach($datea as $key => $value){
                $datea[$key]['datel'] = Carbon::createFromFormat('Y-m-d', $value['date'])->format('d-M-Y');
            }
        }
        return $datea;
    }   
    
    public function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime( $first );
        $last = strtotime( $last );
    
        while( $current <= $last ) {
            $dates[] = ['date'=>date( $format, $current )];
            $current = strtotime( $step, $current );
        }
    
        return $dates;
    }    

    public function timeRange( $first, $last, $step = '+1 hour', $format = 'H' ) {

        $dates = array();
        $current = strtotime( $first );
        $last = strtotime( $last );
    
        while( $current <= $last ) {
            $dates[] = ['date'=>date( $format, $current )];
            $current = strtotime( $step, $current );
        }
    
        return $dates;
    }        

}