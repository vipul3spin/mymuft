<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Mailers\AppMailer;

use App\Models\Order;
use App\Models\User;

class OrdersController extends Controller
{
	public $show_action = true;
	public $view_col = 'order_id';
	//public $listing_cols = ['id', 'order_id', 'choices', 'name', 'cemail', 'mmobno', 'pann', 'panp', 'idp', 'bandw', 'loca', 'staddr', 'addr', 'zipn', 'state', 'city', 'gstn', 'status', 'updated_by'];
	public $listing_cols = ['id','order_id', 'choices', 'name', 'mmobno', 'loca', 'status', 'updated_by'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Orders', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Orders', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Orders.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Orders');
		
		if(Module::hasAccess($module->id)) {
			return View('la.orders.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new order.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created order from Form in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function storeorder(Request $request,AppMailer $mailer)
	{
			$rules = Module::validateRules("Orders", $request);
			
			$validator = Validator::make($request->all(), $rules);

			$flg = false;
			if ($validator->fails()) {
				$flg = true;
				//return redirect()->back()->withErrors($validator)->withInput();
			}

			$lang = $request->lang;
			if($flg){
				$res['statusCode'] = 400;
				if($lang == 'hi'){
					$res['msg'] = "<br/><center><br/><h2>à¤¸à¤­à¥€ à¤µà¤¿à¤µà¤°à¤£ à¤ à¥€à¤• à¤¸à¥‡ à¤­à¤°à¥‡à¤‚</h2></center>";
				} else {
					$res['msg'] = "<br/><center><br/><h2>Error: Fill all details properly</h2></center>";
				}				
			} else {
				$pann = $request->pann;

				if($request->hasFile('panph')){

					$panfile = $request->file('panph');
					$filename = $panfile->getClientOriginalName();
					//$extnpan = $panfile->getMimeType();//pathinfo($request->panp , PATHINFO_EXTENSION);

					$extnpan = substr(strrchr($filename, '.'), 1);

					if($request->hasFile('panph') && ($extnpan == "jpg" || $extnpan == "JPG" || $extnpan == "png" || $extnpan == "png" || $extnpan == "jpeg" || $extnpan == "JPEG" || $extnpan == "pdf" || $extnpan == "PDF")){
						$panpname = $pann.'.'.$extnpan;
						$panName = storage_path('docs').'/'.$panpname;
						move_uploaded_file($request->panph, $panName);
						$request['panp'] = $panpname;
					}
				}
	
				if($request->hasFile('idph')){

					$idfile = $request->file('idph');
					$idfilename = $idfile->getClientOriginalName();

					//$extnpan = $panfile->getMimeType();//pathinfo($request->idp , PATHINFO_EXTENSION);

					$extidp = substr(strrchr($idfilename, '.'), 1);

					if($request->hasFile('idph') && ($extidp == "jpg" || $extidp == "JPG" || $extidp == "png" || $extidp == "png" || $extidp == "jpeg" || $extidp == "JPEG" || $extidp == "pdf" || $extidp == "PDF")){
						$idpname = $pann.'-id.'.$extidp;
						$idName = storage_path('docs').'/'.$idpname;
						move_uploaded_file($request->idph, $idName);
						$request['idp'] = $idpname;
					}
				}

				$chkorder = 1;
				while($chkorder > 0){
					$order_id = "MUFINT".mt_rand(00000, 999999999);
					if($this->chkorderid($order_id)){
						$chkorder = 0;
					} else {
						$chkorder = 1;
					}
				}			

				$request['order_id'] = $order_id;
				$request['status'] = 'Pending';
				$insert_id = Module::insert("Orders", $request);
				$insert_note = $this->addnote($request->mmobno,$order_id);

				$admsubj = 'New order received for '.$request->choices.' from '.$request->name;
				$cutsubj = 'Order for '.$request->choices.' Placed Successfully';

				$mailer->orderadmin($request, $admsubj);
				$mailer->ordercust($request, $cutsubj);

				$res['statusCode'] = 200;
				if($lang == 'hi'){
					$res['msg'] = "<br/><center><br/><h2>à¤¹à¤® à¤¸à¥‡ à¤¸à¥‡à¤µà¤¾ à¤–à¤°à¥€à¤¦à¤¨à¥‡ à¤•à¥‡ à¤²à¤¿à¤ à¤§à¤¨à¥à¤¯à¤µà¤¾à¤¦à¥¤<br/><br/>à¤¹à¤®à¤¾à¤°à¥€ à¤Ÿà¥€à¤® à¤…à¤—à¤²à¥‡ à¥¨à¥ª à¤˜à¤‚à¤Ÿà¥‡ à¤®à¥‡à¤‚ à¤†à¤ªà¤¸à¥‡ à¤¸à¤‚à¤ªà¤°à¥à¤• à¤•à¤°à¥‡à¤—à¥€à¥¤</h2></center>";
				} else {
					$res['msg'] = "<br/><center><br/><h2>Thank you for purchasing service from us.<br/><br/>Our team will reach you in next 24hrs.</h2></center>";
				}	
			}
			
			$data = json_encode($res);
			return $data; 			
	}

	/**
	 * Store a newly created order in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Orders", "create")) {
		
			$rules = Module::validateRules("Orders", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Orders", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.orders.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified order.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Orders", "view")) {
			
			$order = Order::find($id);
			if(isset($order->id)) {
				$module = Module::get('Orders');
				$module->row = $order;
				
				return view('la.orders.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('order', $order);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("order"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified order.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Orders", "edit")) {			
			$order = Order::find($id);
			if(isset($order->id)) {	
				$module = Module::get('Orders');
				
				$module->row = $order;
				
				return view('la.orders.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('order', $order);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("order"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified order in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Orders", "edit")) {
			
			$rules = Module::validateRules("Orders", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Orders", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.orders.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified order from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Orders", "delete")) {
			Order::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.orders.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('orders')->select($this->listing_cols)->whereNull('deleted_at')->orderBy('id','desc');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Orders');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/orders/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }

				if($col == "updated_by") {
					if($data->data[$i][$j] <> ""){
						$uname = User::find($data->data[$i][$j]);
						$data->data[$i][$j] = $uname->name;
					}
				}

			}

			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Orders", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/orders/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Orders", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.orders.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

    public function chkorderid($order_id) {
        if($order_id == NULL){
            return false;
        } else {
            $orderid = DB::table('payments')->where('order_id','=',$order_id)->whereNull('deleted_at')->value('order_id');
            if(empty($orderid)){
                return true;
            } else {
                return false;
            }
        }
    }
	
	public function addnote($mobno,$order_no) {

		$cprofile_id = DB::table('contacts')->where(['mmobno' => $mobno])->whereNull('deleted_at')->orderBy('id','desc')->limit(1)->value('id');

		if($cprofile_id <> null){
			$desc = "Order No.: ".$order_no;
			$time_data = [
				'created_at' => date("Y-m-d H:i:s"),
				'cont_id' => $cprofile_id,
				'title' => "Purchase Order Received",
				'type' => 'note',
				'description' => $desc
			];
			$timeline_ins = DB::table('contact_timelines')->insertGetId($time_data);
		}
	}
}
