<?php
namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use DB;
use Session;
use Excel;
use Carbon\Carbon;
//use App\Models\Pincode;

class ImportController extends Controller
{
  public function index()
  {
    $datas= NULL;
    return view('la.inputfile',['datas' => $datas]);
  }

  public function importExcel(Request $request)
  {
    $this->validate(
      $request,[
        'import_file' => 'required',
        'import_file.*' => 'mimes:json'
    ]);

    $extn = pathinfo($request->import_file , PATHINFO_EXTENSION);

    if($request->hasFile('import_file') || ($extn == "json" || $extn == "JSON")){

    $fpath = Input::file('import_file')->getRealPath(); 
    // Read File

    $jsonString = file_get_contents($fpath);

    $datas = json_decode($jsonString, true);

    foreach($datas as $key => $value) 
    {
      if(is_array($value) || is_object($value))
      {
        foreach($value as $key1 => $value1)
        {
          if(is_array($value1) || is_object($value1))
          { 
            $k1[$key][$key1] = $value1; 
          } else {
            $k2[$key][$key1] = $value1;
          }
        }
      } else {
        $k2[$key] = $value;  
      }
    }

      //dd(array_keys($data));
      dd($k1);
      //return view('la.inputfile')->with(['datas'=>$k2,'datab2b'=>$k1['b2b']]);
      //return back()->with(['success'=>'File Uploaded','datas' => $k2]);
    } else {
      return back()->with('error','Please Select Valid File');
    }

  }

  public function importExcelmain(Request $request)
  {
    $this->validate(
      $request,[
        'import_file' => 'required',
        'import_file.*' => 'mimes:xls,xlsx'
    ]);

    $extn = pathinfo($request->import_file , PATHINFO_EXTENSION);

    echo Input::file('import_file')->getRealPath();

    if($request->hasFile('import_file')  || ($extn == "xls" || $extn == "XLS")){
      if( Input::file('import_file') ) {

          $exclName = storage_path('uploads').'/'.date('d-m-Y').'.xls';

            move_uploaded_file($request->import_file, $exclName);

            Excel::load($exclName, function($reader) use (&$excel) {
                $objExcel = $reader->getExcel();
                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //  Loop through each row of the worksheet in turn
                for ($row = 1; $row <= $highestRow; $row++)
                {
                    //  Read a row of data into an array
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                        NULL, TRUE, FALSE);

                    $excel[] = $rowData[0];
                }
            });

        $header = array_shift($excel);

        $data = array_map(function($v)use($header){
            return array_combine($header, $v);
        }, $excel);


        dd($data);

        // if($request->inputfile == "pincode") {
          
        //   Pincode::query()->truncate();

        //   foreach ($data as $row) { 
        //     Pincode::firstOrCreate($row);
        //   }

        //   return back()->with(['success'=>'Your file successfully import in database!!!','data'=>$data]);
          
        // } else {
        //   return back()->with('error','Please Select Valid File');
        // }
        
        // $path = $request->file('import_file')->getRealPath();
        // $data = Excel::load($path)->get();
        //
        // if($data->count()){
        //     foreach ($data as $key => $value) {
        //         $arr[] = [
        //           'pincode' => $value->pincode,
        //           'areaname' => $value->areaname,
        //           'city' => $value->city,
        //           'state' => $value->state
        //         ];
        //     }
        //
        //     if(!empty($arr)){
        //         //Item::insert($arr);
        //         dd($arr);
        //     }
        // }

      } else {
          return back()->with('error','Please Select Valid File');
      }
    } else {
        return back()->with('error','Please Select Valid File');
    }
  }

  public function importCsv(Request $request)
  {

    $csvName = storage_path('uploads').'/'.date('d-m-Y').'.csv';

    move_uploaded_file($request->import_file, $csvName);

    $contactArr = $this->csvToArray($csvName);

    foreach($contactArr as $key => $value){
      $akey = array_keys($value);

      $list = explode(',', $value['Tags']);

      $cprofile_data = [
        'choice' => $this->servicesel($list[0]),
        'name' => $value['First Name'],
        'email' => $value['Email(default)'],
        'mmobno' => str_replace(' ', '',$value['Phone(default)']),
        'vnamed' => $value['City'],
        'bandwidth' => '',
        'icapacity' => '',
        'vnoc' => '',
        'rcomp' => '',
        'ispl' => '',
        'cname' => $value['Company'],
        'created_at' => Carbon::createFromFormat('m/d/Y', $value['Created Date'])->format('Y-m-d H:i:s')//date("Y-m-d H:i:s")
      ];

      //dd($cprofile_data);

      $cprofile_ins = DB::table('contacts')->insertGetId($cprofile_data);

      if($cprofile_ins){
        if(count($list) > 1){
          foreach($list as $lst => $lval){
            $time_data = [
              'created_at' => Carbon::createFromFormat('m/d/Y', $value['Created Date'])->format('Y-m-d H:i:s'),//date("Y-m-d H:i:s"),
              'cont_id' => $cprofile_ins,
              'title' => $this->servicesel($lval),
              'type' => 'note',
              'description' => ''
            ];
            $timeline_ins = DB::table('contact_timelines')->insertGetId($time_data);
          }
        } else {
            $time_data = [
              'created_at' => Carbon::createFromFormat('m/d/Y', $value['Created Date'])->format('Y-m-d H:i:s'),//date("Y-m-d H:i:s"),
              'cont_id' => $cprofile_ins,
              'title' => $this->servicesel($list[0]),
              'type' => 'note',
              'description' => ''
          ];
          $timeline_ins = DB::table('contact_timelines')->insertGetId($time_data);
        }            
      } 
    }
    return back()->with('success','File Imported Successfully');    
  }

  public function csvToArray($filename = '', $delimiter = ',')
  {
      if (!file_exists($filename) || !is_readable($filename))
          return false;
  
      $header = null;
      $data = array();
      if (($handle = fopen($filename, 'r')) !== false)
      {
          while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
          {
              if (!$header)
                  $header = $row;
              else
                  $data[] = array_combine($header, $row);
          }
          fclose($handle);
      }
  
      return $data;
  }

  public function servicesel($choice){
    //start-business
    //isp-license
    //buy-bandwidth
    //sell-bandwidth
    //Talk to someone
    //ip-1-license
    //as-number-ip-pool        

    switch($choice){
      case 'start-business':
          $cval = "Start a ISP/WISP business (or ISP Franchise)";
          break;
      case 'isp-license':
          $cval = "Apply for a Unified ISP/VNO license";
          break;
      case 'buy-bandwidth':
          $cval = "Buy Bulk Bandwidth";
          break;
      case 'sell-bandwidth':
          $cval = "Sell Bandwidth to ISP Operators";
          break;
      case 'Talk to someone':
          $cval = "Talk to someone at Muft Internet";
          break;
      case 'ip-1-license':
          $cval = "Apply for IP-1 Infrastructure Service Provider License";
          break;
      case 'as-number-ip-pool':
          $cval = "Apply for AS Number / IP Pool";
          break;
      default:
          $cval = $choice;                                     
    }    

    return $cval;

  }

}
