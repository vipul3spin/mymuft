<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Redirect;
use File;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;

use App\Mailers\AppMailer;

use App\Models\Cprofile;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $roleCount = \App\Role::count();
		if($roleCount != 0) {
			if($roleCount != 0) {
                //return view('home');
                return redirect::to('/admin');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }
    
    public function loadform(){ 
        return view('cform');
    }

    public function orderform(){ 
        return view('purchaseform');
    }

    public function test(){

        $ip = '103.145.172.180';
        $port = '22';
        $url = $ip . ':' . $port;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $health = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        dd($health);

        if ($health) {
            $json = json_encode(['health' => $health, 'status' => '1']);
            dd($json);
        } else {
            $json = json_encode(['health' => $health, 'status' => '0']);
            dd($json);
        }

        // $csvData = "vipul@3sp.in,guju339@vipulgmail.com,";
        // if($csvData != ""){
        // $lines = explode(PHP_EOL, $csvData);
        // $array = array();
        // foreach ($lines as $line) {
        //     $array[] = str_getcsv(str_replace(' ', '',$line));
        // }
        // $users_temp = explode(',', $csvData);

        // $array = [];

        // foreach($users_temp as $key => $ut){
      
        //     if(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $ut)){
        //         $array[] = $ut;
        //     }
    
        //   }



        // if(count($array) == 0){
        //     dd("Empty Array");
        // } else {
        //     dd($array);
        // }
        // } else {
        //     dd("Empty");
        // }

        //return view('home');
    }

    public function dform(Request $request){
        $lang = $request->lang;
        $fname = $request->id;
        $cont = File::get(storage_path('form/'.$lang.'/'.$fname.'.txt'));
        return $cont;
    }

    public function store(Request $request,AppMailer $mailer){

            // collect value of input field
            
            $choices = $request->choices;
        
            switch($choices){
                case 1:
                    $cval = "Start a ISP/WISP business (or ISP Franchise)";
                    break;
                case 2:
                    $cval = "Apply for a Unified ISP/VNO license";
                    break;
                case 3:
                    $cval = "Buy Bulk Bandwidth";
                    break;
                case 4:
                    $cval = "Sell Bandwidth to ISP Operators";
                    break;
                case 5:
                    $cval = "Talk to someone at Muft Internet";
                    break;
                case 6:
                    $cval = "Apply for IP-1 Infrastructure Service Provider License";
                    break;
                case 7:
                    $cval = "Apply for AS Number / IP Pool";
                    break;
                default:
                    $cval = "Not Selected";                                     
            }

            $lang = $request->lang;

            $error = false;           

            $validate_name = Validator::make($request->all(), ['name'=> 'required|min:4']);
            if($validate_name->fails()){
                    $res['statusCode'] = 400;
                    if($lang == 'hi'){
                        $res['errorname'] = "अपना पूरा नाम दर्ज करें";
                    } else {
                        $res['errorname'] = "Please enter your full name";
                    }

                    // $data = json_encode($res);
                    // return $data; 
                    $error = true;
            }
            
            $validate_email = Validator::make($request->all(), ['email'=> 'required|email']);
            if($validate_email->fails()){
                    $res['statusCode'] = 400;
                    if($lang == 'hi'){
                        $res['erroremails'] = "कृपया मान्य ईमेल आईडी दर्ज करें";
                    } else {
                        $res['erroremails'] = "Please enter valid email id";
                    }
                    // $data = json_encode($res);
                    // return $data; 
                    $error = true;
            } 

            $validate_mno = Validator::make($request->all(), ['mmobno'=> 'required|numeric|min:11111']);
            if($validate_mno->fails()){
                    $res['statusCode'] = 400;
                    if($lang == 'hi'){
                        $res['errormobileno'] = "कृपया मान्य फ़ोन नंबर दर्ज करें";
                    } else {
                        $res['errormobileno'] = "Please enter valid phone number";
                    }
                    // $data = json_encode($res);
                    // return $data;  
                    $error = true;
            } elseif(strlen($request->mmobno) != 10) {
                    $res['statusCode'] = 400;
                    if($lang == 'hi'){
                        $res['errormobileno'] = "कृपया मान्य 10 अंकों का मोबाइल नंबर दर्ज करें";
                    } else {
                        $res['errormobileno'] = "Please enter valid 10 digit mobile number";
                    }
                    // $data = json_encode($res);
                    // return $data;  
                    $error = true;            
            } 


            $tim_descr = '';

            if($lang == 'hi'){
                $tim_descr .= '<b>Language</b> : Hindi<br/>';
            } else {
                $tim_descr .= '<b>Language</b> : English<br/>';
            }

            $tim_descr .= '<b>Service Required</b> : '.$cval.'<br/>';
            //1
            //$ispplan;
            //$vnamed;
            //$icapacity;
            if(isset($request->ispplan)){
                $validate_ispplan = Validator::make($request->all(), ['ispplan'=> 'required|numeric']);
                if($validate_ispplan->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['errorispplan'] = "कृपया अपना विकल्प चुनें";
                        } else {
                            $res['errorispplan'] = "Please select option";
                        }

                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {
                    $ispplan = $request->ispplan;
                    if($ispplan == 1){
                        $tim_descr .= '<b>Start Business </b> : Within 1 - 2 Months<br/>';
                    } elseif( $ispplan == 2){
                        $tim_descr .= '<b>Start Business </b> : Within 2 - 5 Months<br/>';
                    } else {
                        $tim_descr .= '<b>Start Business </b> : After 5 Months<br/>';
                    }
                }
            } else {
                $ispplan = "N/A";
            }            
            if(isset($request->vnamed)){
                $validate_city = Validator::make($request->all(), ['vnamed'=> 'required|min:4']);
                if($validate_city->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['errorvnamed'] = "कृपया शहर का नाम दर्ज करें";
                        } else {
                            $res['errorvnamed'] = "Please enter city name";
                        }
                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {
                    $vnamed = $request->vnamed;
                    $tim_descr .= '<b>Service Location</b> : '.$vnamed.'<br/>';                
                }
            } else {
                $vnamed = "N/A";
            }
            if(isset($request->icapacity)){
                $validate_icapacity = Validator::make($request->all(), ['icapacity'=> 'required|numeric']);
                if($validate_icapacity->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['erroricapacity'] = "कृपया निवेश क्षमता का चयन करें";
                        } else {
                            $res['erroricapacity'] = "Please select investment capacity";
                        }

                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {
                    $icapacity = $request->icapacity;
                    $tim_descr .= '<b>Investment Capacity</b> : INR.'.$icapacity.'<br/>';
                }
            } else {
                $icapacity = "N/A";
            }
            //2
            //$vnoc;
            //$rcomp;
            if(isset($request->vnoc)){
                $validate_vnoc = Validator::make($request->all(), ['vnoc'=> 'required|min:2']);
                if($validate_vnoc->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['errorvnoc'] = "कृपया श्रेणी का चयन करें";
                        } else {
                            $res['errorvnoc'] = "Please select category";
                        }
                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {
                    $vnoc = $request->vnoc;
                    $tim_descr .= '<b>ISP / VNO Category</b> : '.$vnoc.'<br/>';
                }
            } else {
                $vnoc = "N/A";
            }
            if(isset($request->rcomp)){
                $validate_rcomp = Validator::make($request->all(), ['rcomp'=> 'required|min:2']);
                if($validate_rcomp->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['errorrcomp'] = "कृपया अपना विकल्प चुनें";
                        } else {
                            $res['errorrcomp'] = "Please select option";
                        }
                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {                
                    $rcomp = $request->rcomp;
                    $tim_descr .= '<b>Registred Company</b> : '.$rcomp.'<br/>';
                }
            } else {
                $rcomp = "N/A";
            }
        
            //3
            //$eisp;
            //$bandwidth;
            //$vnamed;
            //$ispl;
            if(isset($request->eisp)){
                $validate_eisp = Validator::make($request->all(), ['eisp'=> 'required|min:2']);
                if($validate_eisp->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['errorreisp'] = "कृपया अपना विकल्प चुनें";
                        } else {
                            $res['errorreisp'] = "Please select option";
                        }
                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {                
                    $eisp = $request->eisp;
                    $tim_descr .= '<b>Existing ISP</b> : '.$eisp.'<br/>';
                }
            } else {
                $eisp = "N/A";
            }

            if(isset($request->bandwidth)){
                $validate_bandwidth = Validator::make($request->all(), ['bandwidth'=> 'required|numeric|min:49']);
                if($validate_bandwidth->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['errorrbandwidth'] = "कृपया बैंडविड्थ दर्ज करें (न्यूनतम ५० Mbps)";
                        } else {
                            $res['errorrbandwidth'] = "Please enter bandwidth (Minimum 50 Mbps)";
                        }
                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {                
                    $bandwidth = $request->bandwidth." Mbps";
                    $tim_descr .= '<b>Bandwidth</b> : '.$bandwidth.'<br/>';
                }
            } else {
                $bandwidth = "N/A";
            }
        
            if(isset($request->ispl)){
                $validate_ispl = Validator::make($request->all(), ['ispl'=> 'required|min:2']);
                if($validate_ispl->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['errorrispl'] = "कृपया अपना विकल्प चुनें";
                        } else {
                            $res['errorrispl'] = "Please select option";
                        }
                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {
                    $ispl = $request->ispl;
                    $tim_descr .= '<b>ISP Licence</b> : '.$ispl.'<br/>';
                }
            } else {
                $ispl = "N/A";
            }
        
            //4
            //$cname;
            //$vnamed;
            if(isset($request->cname)){
                $validate_cname = Validator::make($request->all(), ['cname'=> 'required|min:4']);
                if($validate_cname->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['errorcname'] = "कृपया कंपनी का नाम दर्ज करें";
                        } else {
                            $res['errorcname'] = "Please enter company name";
                        }
                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {
                    $cname = $request->cname;
                    $tim_descr .= '<b>Company</b> : '.$cname.'<br/>';
                }
            } else {
                $cname = "N/A";
            }

            //5
            //$badd;
            //$query;
            if(isset($request->badd)){
                $validate_badd = Validator::make($request->all(), ['badd'=> 'required|min:4']);
                if($validate_badd->fails()){
                        $res['statusCode'] = 400;
                        if($lang == 'hi'){
                            $res['errorbadd'] = "कृपया व्यवसाय का पता दर्ज करें";
                        } else {
                            $res['errorbadd'] = "Please enter business address";
                        }
                        // $data = json_encode($res);
                        // return $data; 
                        $error = true;
                } else {
                    $badd = $request->badd;
                    $tim_descr .= '<b>Business address</b> : '.$badd.'<br/>';
                }
            } else {
                $badd = "N/A";
            }

            if(isset($request->custquery)){
                if($choices == 5){
                    $validate_query = Validator::make($request->all(), ['custquery'=> 'required|min:10']);
                    if($validate_query->fails()){
                            $res['statusCode'] = 400;
                            if($lang == 'hi'){
                                $res['errorquery'] = "कृपया टिप्पणी / मूद्दा दर्ज करें (न्यूनतम १० शब्द)";
                            } else {
                                $res['errorquery'] = "Please enter query (Min. 10 Words)";
                            }
                            // $data = json_encode($res);
                            // return $data; 
                            $error = true;
                    } else {
                        $custquery = $request->custquery;
                        $tim_descr .= '<b>Query</b> : '.$custquery.'<br/>';
                    }
                } elseif($request->custquery != ""){
                    $custquery = $request->custquery;
                    $tim_descr .= '<b>Note</b> : '.$custquery.'<br/>';                    
                }
            } else {
                $query = "N/A";
            }            


            //6
            //$rcomp;
            //$cname;
        
            //7
            //$ispl;
            //$cname;

        $validator = Validator::make($request->all(), ['name'=> 'required','email' => 'required', 'mmobno' => 'required']);
        
        if(!$error){
            if ($validator->fails()) {
                $res['statusCode'] = 400;
                if($lang == 'hi'){
                    $res['msg'] = "कृपया सभी फ़ील्ड भरें";
                } else {
                    $res['msg'] = "Please fill all the fields";
                }
            } else {         

                $ip = $_SERVER['REMOTE_ADDR'];
                $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}?token=f0b529b1f92b1e"));
                $geolocation = '';
                if(isset($details->city)){
                    $geolocation .= $details->city.", ";
                } 
                if(isset($details->region)){
                    $geolocation .= $details->region.", ";
                } 
                if(isset($details->country)){
                    $geolocation .= $details->country;
                } 

                $cprofile_data = [
                    'choice' => $cval,
                    'name' => $request->name,
                    'email' => $request->email,
                    'mmobno' => $request->mmobno,
                    'vnamed' => $geolocation,
                    'bandwidth' => $bandwidth,
                    'icapacity' => $icapacity,
                    'vnoc' => $vnoc,
                    'rcomp' => $rcomp,
                    'ispl' => $ispl,
                    'cname' => $cname,
                    'created_at' => date("Y-m-d H:i:s")
                    ];
                
                $cprofile_ins = DB::table('contacts')->insertGetId($cprofile_data);

                if($cprofile_ins){
                    $time_data = [
                        'created_at' => date("Y-m-d H:i:s"),
                        'cont_id' => $cprofile_ins,
                        'title' => $cval,
                        'type' => 'note',
                        'description' => $tim_descr
                    ];
                    $timeline_ins = DB::table('contact_timelines')->insertGetId($time_data);
                }

                $bdata = '<b>Name</b> : '.$request->name.'<br/>';
                $bdata .= '<b>Email</b> : '.$request->email.'<br/>';
                $bdata .= '<b>Mobile Number</b> : '.$request->mmobno.'<br/>';
                $bdata .= '<b>Location</b> : '.$geolocation.'<br/>';
                $bdata .= $tim_descr;

				$subj = $cval;
				$mail_body = $bdata;
				$mailer->sendadminmail($mail_body, $subj);

                $res['statusCode'] = 200;
                if($lang == 'hi'){
                    $res['msg'] = "<br/><center><br/><h2>हमसे संपर्क करने के लिए आपका धन्यवाद।<br/><br/>हमारी टीम अगले २४ घंटे में आपसे संपर्क करेगी।</h2></center>";
                } else {
                    $res['msg'] = "<br/><center><br/><h2>Thank you for contacting us.<br/><br/>Our team will reach you in next 24hrs.</h2></center>";
                }
            }
        }
            //echo "</br><center><h2>Thank you for contacting us.</br>Our team will reach you in next 24hrs.</h2></center></br>";          
          $data = json_encode($res);
          return $data;        
    }
}