<?php

/* ================== Homepage ================== */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get('/contact','HomeController@loadform');
Route::get('/contact/{lang}/{id}','HomeController@dform');
Route::post('/contact','HomeController@store');
Route::get('/purchase-order','HomeController@orderform');
Route::post('/purchase-order','LA\OrdersController@storeorder');

//test route for sample
Route::get('/test1', 'LA\OrdersController@addnote');
Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

Route::get('docs/{filename}', function ($filename)
{
    $path = storage_path('docs/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {

	/* ================== Clear Cache ================ */

	Route::get(config('laraadmin.adminRoute'). '/clear-cache', function() {
		Artisan::call('cache:clear');
		//Artisan::call('route:cache');
		Artisan::call('config:cache');
		Artisan::call('view:clear');
		return back()->with('success','All Cache Cleared Successfully');
	});	
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');

	/* ================== Switch User ================== */
	Route::get(config('laraadmin.adminRoute').'/switch/start/{id}', 'LA\UsersController@user_switch_start');
	Route::get(config('laraadmin.adminRoute'). '/switch/stop', 'LA\UsersController@user_switch_stop');	
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_e_password/{id}', 'LA\EmployeesController@change_password');
	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');

	/* ================== Customers ================== */
	Route::resource(config('laraadmin.adminRoute') . '/customers', 'LA\CustomersController');
	Route::get(config('laraadmin.adminRoute') . '/customer_dt_ajax', 'LA\CustomersController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_c_password/{id}', 'LA\CustomersController@change_password');

	/* ================== Support ================== */
	Route::resource(config('laraadmin.adminRoute') . '/support', 'LA\SupportController');	

	/* ================== Ticket_Categories ================== */
	Route::resource(config('laraadmin.adminRoute') . '/ticket_categories', 'LA\Ticket_CategoriesController');
	Route::get(config('laraadmin.adminRoute') . '/ticket_category_dt_ajax', 'LA\Ticket_CategoriesController@dtajax');

	/* ================== Tickets ================== */
	Route::resource(config('laraadmin.adminRoute') . '/tickets', 'LA\TicketsController');
	Route::get(config('laraadmin.adminRoute') . '/ticket_dt_ajax', 'LA\TicketsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/close', 'LA\TicketsController@close');

	/* ================== Comments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/comments', 'LA\CommentsController');
	Route::get(config('laraadmin.adminRoute') . '/comment_dt_ajax', 'LA\CommentsController@dtajax');

	/* ================== Customer_Types ================== */
	Route::resource(config('laraadmin.adminRoute') . '/customer_types', 'LA\Customer_TypesController');
	Route::get(config('laraadmin.adminRoute') . '/customer_type_dt_ajax', 'LA\Customer_TypesController@dtajax');

	/* ================== Pricing_Plans ================== */
	Route::resource(config('laraadmin.adminRoute') . '/pricing_plans', 'LA\Pricing_PlansController');
	Route::get(config('laraadmin.adminRoute') . '/pricing_plan_dt_ajax', 'LA\Pricing_PlansController@dtajax');

	/* ================== Subscriptions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/subscriptions', 'LA\SubscriptionsController');
	Route::get(config('laraadmin.adminRoute') . '/subscription_dt_ajax', 'LA\SubscriptionsController@dtajax');

	/* ================== Payments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/payments', 'LA\PaymentsController');
	Route::get(config('laraadmin.adminRoute') . '/payment_dt_ajax', 'LA\PaymentsController@dtajax');

	/* ================== Make Payment Request  ================== */
	Route::resource(config('laraadmin.adminRoute') . '/purchase_plan', 'LA\Purchase_PlanController@purplan');
	Route::post(config('laraadmin.adminRoute') .'/payment', 'LA\RazorpayController@payment')->name('payment');
	//Route::get(config('laraadmin.adminRoute') . '/test', 'LA\RazorpayController@vieworder');		
	
	/* ================== Contacts Leads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/contacts', 'LA\ContactsController');
	Route::get(config('laraadmin.adminRoute') . '/contact_dt_ajax', 'LA\ContactsController@dtajax');

	/* ================== Contact_Timelines ================== */
	Route::resource(config('laraadmin.adminRoute') . '/contact_timelines', 'LA\Contact_TimelinesController');
	Route::get(config('laraadmin.adminRoute') . '/contact_timeline_dt_ajax', 'LA\Contact_TimelinesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/contact_timelines_add', 'LA\Contact_TimelinesController@addcomment');	

	/* ================== Email_Templates ================== */
	Route::resource(config('laraadmin.adminRoute') . '/email_templates', 'LA\Email_TemplatesController');
	Route::get(config('laraadmin.adminRoute') . '/email_template_dt_ajax', 'LA\Email_TemplatesController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/get_email_templates', 'LA\Email_TemplatesController@gettempall');
	Route::get(config('laraadmin.adminRoute') . '/get_email_template/{id}', 'LA\Email_TemplatesController@gettemp');

	/* ======== EXCEL file import ========*/
	Route::get(config('laraadmin.adminRoute') . '/inputfile', 'LA\ImportController@index');
	Route::post(config('laraadmin.adminRoute') . '/inputfile', 'LA\ImportController@importCsv');

	/* ================== Email_Categories ================== */
	Route::resource(config('laraadmin.adminRoute') . '/email_categories', 'LA\Email_CategoriesController');
	Route::get(config('laraadmin.adminRoute') . '/email_category_dt_ajax', 'LA\Email_CategoriesController@dtajax');  
	
	/* ================== Email_Stats ================== */
	Route::get(config('laraadmin.adminRoute') . '/email_stats', 'LA\Email_StatsController@index');
	Route::get(config('laraadmin.adminRoute') . '/email_stats_dt_ajax', 'LA\Email_StatsController@dtajax'); 	

    /* ================== Orders ================== */
	Route::resource(config('laraadmin.adminRoute') . '/orders', 'LA\OrdersController');
	Route::get(config('laraadmin.adminRoute') . '/order_dt_ajax', 'LA\OrdersController@dtajax');
});
