<div class="form-group">
    <label class="form-label">Which ISP or VNO authorization would you like to apply for?* :</label>
    <select name="vnoc" class="form-control">
        <option value="0">Select</option>
        <option value="Class A">Category A</option>
        <option value="Class B">Category B</option>
        <option value="Class C">Category C</option>
        <option value="Not sure">I'm not sure</option>
    </select>
    <span id="err-vnoc" class="text-danger"></span>
</div>
<div class="form-group">
    <label class="form-label">Do you have a registered Private Limted (Pvt. Ltd) or Public Limited (Ltd) company in India?* :</label>
    <select name="rcomp" id="rcomp" class="form-control" onchange="getcompname(this.value)">
        <option value="0">Select</option>
        <option value="Yes">Yes</option>
        <option value="No">No</option>
        <option value="Not Sure">I'm not sure</option>
    </select>
    <span id="err-rcomp" class="text-danger"></span>
</div>
<div class="form-group" id="compname" style="display:none;">
    <label class="form-label">Your Company Name* :</label> 
    <input name="cname" value="" placeholder="Enter company name" required class="form-control">
    <span id="err-cname" class="text-danger"></span>
</div>