<!-- /.panel-heading -->
<div class="box-header">
    <h3 class="box-title" id="box-title" style="display:inline-block;">Purchase Request</h3>
</div>
<div class="panel-body"> 
    <div class="form-group">
        <input type="hidden" name="lang" value="en">
        <label class="form-label">I am interested in purchasing:</label>
        <select id="selchoices" onchange="getservice(this.value)" class="form-control">
            <option value="0">Select Service</option>
            <option value="Bandwidth - Bulk Internet" selected>Bandwidth - Bulk Internet</option>
            <!-- <option value="NLD">NLD</option>
            <option value="Peering">Peering</option> -->
        </select>
        <span id="err-choices" class="text-danger"></span>
        <input type="hidden" name="choices" id="choices" value="">
    </div>

<!--	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist" >
		<li class="active"><a role="tab" data-toggle="tab" class="active" id="atab-info" href="#tab-info" data-target="#tab-info"> 1. Contact Details</a></li>
		<li class=""><a role="tab" data-toggle="tab" id="atab-idproof" href="#tab-idproof" data-target="#tab-idproof"> 2. ID Proof</a></li>
        <li class=""><a role="tab" data-toggle="tab" id="atab-deladdr" href="#tab-deladdr" data-target="#tab-deladdr"> 3. Delivery Address</a></li>
        <li class=""><a role="tab" data-toggle="tab" id="atab-billdet" href="#tab-billdet" data-target="#tab-billdet"> 4. Billing Details</a></li>
	</ul> 
-->
	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist" >
		<li class="active"><a role="tab" data-toggle="tab" class="active" id="atab-info" href="#tab-info" data-target="#tab-info"> 1. Contact Details</a></li>
		<li class=""><a role="tab" id="atab-idproof"> 2. ID Proof</a></li>
        <li class=""><a role="tab" id="atab-deladdr"> 3. Delivery Address</a></li>
        <li class=""><a role="tab" id="atab-billdet"> 4. Billing Details</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-body">

                        <div class="form-group" id="name">
                            <label class="form-label">Your Name* :</label> 
                            <input type="text" name="name" value="" placeholder="Enter your full name" required class="form-control">
                            <span id="err-name" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="cemail" >
                            <label class="form-label">Company Email Address* :</label>
                            <input type="email" name="cemail" value="" placeholder="Nice to meet you! Don't worry, we won't spam you!" required class="form-control">
                            <span id="err-cemail" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="mmobno">
                            <label class="form-label">Your Mobile Number* :</label>
                            <input type="number" name="mmobno" value="" placeholder="Please enter your 10 digit mobile number (do not enter country code)" required class="form-control">
                            <span id="err-mmobno" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="btn1">
                            <a onclick="gototab('atab-info','atab-idproof')" class="btn btn-success">Next</a>
                        </div>                              						
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade in" id="tab-idproof">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-body">
                        <div class="form-group" id="pann">
                            <label class="form-label">PAN No.* :</label> 
                            <input type="text" name="pann" value="" placeholder="Please enter your individual or company PAN number." required class="form-control" oninput="this.value = this.value.toUpperCase()">
                            <span id="err-pann" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="panp" >
                            <label class="form-label">Pan Card Photocopy* :</label>
                            <input type="file" name="panph" value="" placeholder="Please upload your individual or company PAN number ( jpg, png, pdf)" class="form-control">
                            <span id="err-panp" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="idp">
                            <label class="form-label">Aadhar card / driving licence / Voter Card* :</label>
                            <input type="file" name="idph" value="" placeholder="Please upload a photocopy of your personal Aadhar card / Driving Licence / Voter Card ( jpg, png, pdf)" class="form-control">
                            <span id="err-idp" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="btn2">
                            <a class="btn btn-success" onclick="gototab('atab-idproof','atab-info')">Previous</a>
                            <a class="btn btn-success" onclick="gototab('atab-idproof','atab-deladdr')">Next</a>
                        </div> 						
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade in" id="tab-deladdr">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-body">
                        <div class="form-group" id="bandw">
                            <label class="form-label">How many Mbps( Min.50Mbps)?* :</label> 
                            <input type="number" name="bandw" value="" placeholder="Enter the amount of data transfer your would like to purchase" required class="form-control">
                            <span id="err-bandw" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="loca" >
                            <label class="form-label">Where do you need the drop service(BTS code & Address)?* :</label>
                            <input type="text" name="loca" value="" placeholder="Enter service area location" required class="form-control">
                            <span id="err-loca" class="text-danger"></span>
                        </div>	
                        <div class="form-group" id="btn3">
                            <a class="btn btn-success" onclick="gototab('atab-deladdr','atab-idproof')">Previous</a>
                            <a class="btn btn-success" onclick="gototab('atab-deladdr','atab-billdet')">Next</a>
                        </div> 	                        					
					</div>
				</div>
			</div>
		</div>        
		<div role="tabpanel" class="tab-pane fade in" id="tab-billdet">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-body">
                        <div class="form-group" id="staddr">
                            <label class="form-label">Street Address* :</label> 
                            <input type="text" name="staddr" value="" placeholder="Enter Street Address" required class="form-control">
                            <span id="err-staddr" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="addr">
                            <label class="form-label">Address Line 2* :</label> 
                            <input type="text" name="addr" value="" placeholder="Address" required class="form-control">
                            <span id="err-addr" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="zipn">
                            <label class="form-label">Zip / Postal Code* :</label> 
                            <input type="text" name="zipn" value="" placeholder="Enter Zip/Postal Code" required class="form-control">
                            <span id="err-zipn" class="text-danger"></span>
                        </div>                         
                        <div class="form-group" id="state-m">
                            <label class="form-label">State* :</label> 
                            <select onchange="print_city('city', this.selectedIndex);" id="state" name ="state" class="form-control" required>
                            </select>
                            <span id="err-state" class="text-danger"></span>
                        </div> 
                        <div class="form-group" id="city-m">
                            <label class="form-label">City* :</label>
                            <select id ="city" name="city" class="form-control" required>
                                <option value="">Select State First</option>
                            </select>
                            <span id="err-city" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="gstn">
                            <label class="form-label">GST No. :</label> 
                            <input type="text" name="gstn" value="" placeholder="Please enter GST no." class="form-control">
                            <span id="err-gstn" class="text-danger"></span>
                        </div>
                        <div class="form-group" id="sbtn">
                            <a class="btn btn-success" onclick="gototab('atab-billdet','atab-deladdr')">Previous</a>
                            <button class="btn btn-success">Submit</button>
                        </div> 	                                                                                                                                            						
					</div>
				</div>
			</div>
		</div>		
	</div>    
</div>                  