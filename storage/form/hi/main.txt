﻿                            <!-- /.panel-heading -->
                            <div class="box-header">
                                <h3 class="box-title" id="box-title" style="display:inline-block;">हम आपकी कैसे मदद कर सकते हैं?</h3>
                            </div>
                            <div class="panel-body">  
				<input type="hidden" name="lang" value="hi">
				<div class="form-group">
                                <label class="form-label">मैं चाहता हूं:</label>
                                <select id="selchoices" onchange="getservice(this.value)" class="form-control">
                                  <option value="0">सेवा का चयन करें</option>
                                  <option value="1">ISP / WISP व्यवसाय (या ISP फ़्रेंचाइज़) शुरू करें</option>
                                  <option value="2">यूनिफाइड ISP / VNO लाइसेंस के लिए आवेदन करें</option>
                                  <option value="3">थोक बैंडविड्थ खरीदें</option>
                                  <option value="4">ISP ऑपरेटर्स को बैंडविड्थ बेचें</option>
                                  <option value="5">मुफ़्त इंटरनेट में किसी से बात</option>
                                  <option value="6">IP-1 इन्फ्रास्ट्रक्चर सर्विस प्रोवाइडर लाइसेंस के लिए आवेदन करें</option>
                                  <option value="7">AS नंबर / IP पूल के लिए आवेदन करें</option>
                                </select>
                                <input type="hidden" name="choices" id="choices" value="">
                              </div>
                              <div id="demo"></div>
                              <div class="form-group" id="name">
                                  <label class="form-label">आपका नाम* :</label> 
                                  <input type="text" name="name" value="" placeholder="अपना पूरा नाम दर्ज करें" required class="form-control">
                                  <span id="err-name" class="text-danger"></span>
                              </div>
                              <div class="form-group" id="email" >
                                  <label class="form-label">आपका ईमेल पता* :</label>
                                  <input type="email" name="email" value="" placeholder="अपना ईमेल पता दर्ज करें" required class="form-control">
                                  <span id="err-email" class="text-danger"></span>
                              </div>
                              <div class="form-group" id="mmobno">
                                  <label class="form-label">आपका फोन नंबर* :</label>
                                  <input type="number" name="mmobno" value="" placeholder="अपना फ़ोन नंबर दर्ज करें" required class="form-control">
                                  <span id="err-mmobno" class="text-danger"></span>
                              </div>
                              <div class="form-group" id="snote">
                                    <label class="form-label">टिप्पणी :</label>
                                    <textarea name="custquery" value="" placeholder="अन्य कोई सूचना" class="form-control"></textarea>
                                    <span id="err-query" class="text-danger"></span>
                                </div>
                              <div class="form-group" id="sbtn">
                                  <button class="btn btn-success">जमा करें</button>
                              </div>
                            </div> 