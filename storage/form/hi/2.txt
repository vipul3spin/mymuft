﻿<div class="form-group">
    <label class="form-label">आप किस ISP या VNO प्राधिकरण के लिए आवेदन करना चाहेंगे?* :</label>
    <select name="vnoc" class="form-control">
        <option value="0">चयन करें</option>
        <option value="Class A">श्रेणी A</option>
        <option value="Class B">श्रेणी B</option>
        <option value="Class C">श्रेणी C</option>
        <option value="Not sure">मुझे यकीन नहीं है</option>
    </select>
    <span id="err-vnoc" class="text-danger"></span>
</div>
<div class="form-group">
    <label class="form-label">क्या आपके पास भारत में रजिस्टर्ड प्राइवेट लिमिटेड या पब्लिक लिमिटेड कंपनी है?* :</label>
    <select name="rcomp" class="form-control" id="rcomp" onchange="getcompname(this.value)">
        <option value="0">चयन करें</option>
        <option value="Yes">हाँ</option>
        <option value="No">नहीं</option>
        <option value="Not Sure">मुझे यकीन नहीं है</option>
    </select>
    <span id="err-rcomp" class="text-danger"></span>
</div>
<div class="form-group" id="compname" style="display:none;">
    <label class="form-label">आपकी कंपनी का नाम* :</label> 
    <input name="cname" value="" placeholder="कंपनी का नाम दर्ज करें" required class="form-control">
    <span id="err-cname" class="text-danger"></span>
</div>